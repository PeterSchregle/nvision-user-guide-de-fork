Eine Gruppe verbundener Pixel wird üblicherweise mit dem Begriff Blob
bezeichnet. Hier ist ein Beispiel für ein Bild, das ein paar Blobs
enthält. Das Originalbild wird segmentiert, und die verbundenen Pixel
werden bestimmt und deren Zusammegehörigkeit farbig visualisiert.

<table>
<colgroup>
<col width="26%" />
<col width="29%" />
<col width="37%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Originalbild</th>
<th align="left">Segmentiertes Bild</th>
<th align="left">Verbundene Komponenten</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="images/blob_analysis_eggs_original.png" alt="image3" /></td>
<td align="left"><img src="images/blob_analysis_eggs_thresholded.png" alt="image4" /></td>
<td align="left"><img src="images/blob_analysis_eggs_labelled.png" alt="image5" /></td>
</tr>
</tbody>
</table>

Blob Analyse oder auch Partikelanalyse nimmt einen Satz von verbundenen
Pixeln entgegen, die üblicherweise durch die Aufnahme und
Vorverarbeitung von realen Objekten erzeugt werden, und berechtet
Messwerte für diese Pixel-Sätze, wie z.B. deren Fläche, Position, etc.

Der kleinstmögliche Blob besteht aus einem einzelnen Pixel, größere
Blobs bestehen aus mehreren Pixeln.

![Enhält dieses Bild einen oder zwei Blobs? Es hängt davon ab, wie man
diagonale Verbindungen interpretiert. Wenn die Blobs 4-connected sind,
dann sind es zwei Blobs. Wenn die Blobs 8-connected sind, dann ist es
nur ein Blob.](images/blob_analysis_one_or_two.png)

Betrachten Sie das obige Bild: enthält es einen oder zwei Blobs? Das
hängt davon ab, wie Sie es sehen wollen. Wenn nur horizontale oder
vertikale Nachbarschaft eine Verbindungen bestimmen, dann enthält das
Bild zwei Blobs. Wenn jedoch auch diagonale Verbindungen erlaubt sind,
dann enthält das Bild nur einen Blob.

<table>
<colgroup>
<col width="27%" />
<col width="27%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">4er Nachbarschaft</th>
<th align="left">8er Nachbarschaft</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="images/blob_analysis_4_connected.png" alt="image8" /></td>
<td align="left"><img src="images/blob_analysis_8_connected.png" alt="image9" /></td>
</tr>
</tbody>
</table>

Oftmals wird die 8er Nachbarschaft für den Vordergrund und die 4er
Nachbarschaft für den Hintergrund verwendet. Mit dieser Konvention
enthält das folgende Bild ein weißes Objekte mit zwei schwarzen Löchern.

![Ein weißes Objekt mit zwei schwarzen
Löchern.](images/blob_analysis_some_objects.png)

Die Beziehungen in diesem Bild können mit der folgenden Tabelle
verdeutlicht werden-

Die Tabelle zeigt klar, dass es nur zwei nützliche
Nachbarschaftskonfigurationen gibt:

<table>
<colgroup>
<col width="37%" />
<col width="37%" />
<col width="24%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Nachbarschaft Vordergrund</th>
<th align="left">Nachbarschaft Hintergrund</th>
<th align="left">Objekte/Löcher</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">4</td>
<td align="left">4</td>
<td align="left">7/2</td>
</tr>
<tr class="even">
<td align="left">4</td>
<td align="left">8</td>
<td align="left"><strong>7/0</strong></td>
</tr>
<tr class="odd">
<td align="left">8</td>
<td align="left">4</td>
<td align="left"><strong>1/2</strong></td>
</tr>
<tr class="even">
<td align="left">8</td>
<td align="left">8</td>
<td align="left">1/0</td>
</tr>
</tbody>
</table>

-   Ein (weißes) Objekt mit zwei (schwarzen) Löchern, oder
-   Sieben (weiße) Objekte ohne Löcher.

Die anderen möglichen Konfigurationen machen einfach keinen Sinn, egal
wie man sie auch betrachten will.

Standardmäßig erwartet **nVision**, dass Vordergrund Pixel eine 8er
Nachbarschaft haben, und Hintergrund Pixel eine 4er Nachbarschaft.

**nVision** verwendet Regionen um Blobs zu repräsentieren. Blobanalyse
ist der Prozess, der aus Regionen Merkmale berechnet. Aufgrund der
effizienten Lauflängen-Kodierung von Regionen können diese Merkmale sehr
schnell berechnet werden. Da eine Region keine Pixelwerte besitzt, muß
dazu nicht auf Pixel zugegriffen werden. Eine überraschend große Zahl
von Eigenschaften kann ohne Pixelwerte berechnet werden, wie
beispielsweise der Flächenschwerpunkt. Das Beispiel zeigt, wie eine
Liste von Regionen durch Threshold an einem Schwellwert und Connected
Components erzeugt und analysiert wird:

![Segmentierung, Zusammenhangs- und
Blob-Analyse.](images/threshold_cc.png)

Es gibt jedoch auch eine Klasse von Merkmalen, die nur berechnet werden
können, wenn auch die Pixelwerte berücksichtigt werden, wie die mittlere
Helligkeit eines Blobs. **nVision** hat einen Satz von Funktionen, die
auch Pixel-basierte Merkmale berechnen können, aber mit etwas höherem
Zeitaufwand. Auch die Pixel-basierte Merkmalsberechnung benötigt eine
Region um das Objekt zu definieren.

Here ist eine Liste der Regionen Merkmale:

<table>
<colgroup>
<col width="23%" />
<col width="76%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Merkmal</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Area</td>
<td align="left">Die Fläche als Anzahl der Pixel.</td>
</tr>
<tr class="even">
<td align="left">Left</td>
<td align="left">Die linke Spaltenposition (inklusiv).</td>
</tr>
<tr class="odd">
<td align="left">Right</td>
<td align="left">Die rechte Spaltenposition (exklusiv).</td>
</tr>
<tr class="even">
<td align="left">Top</td>
<td align="left">Die obere Zeilenposition (inklusiv).</td>
</tr>
<tr class="odd">
<td align="left">Bottom</td>
<td align="left">Die untere Zeilenposition (exklusiv).</td>
</tr>
<tr class="even">
<td align="left">Width</td>
<td align="left">Die achsenparallele Breite.</td>
</tr>
<tr class="odd">
<td align="left">Height</td>
<td align="left">Die achsenparallele Höhe.</td>
</tr>
<tr class="even">
<td align="left">Aspect Ratio</td>
<td align="left">Das Seitenverhältnis (achsenparallel).</td>
</tr>
<tr class="odd">
<td align="left">Bounds</td>
<td align="left">Das umgebende Rechteck (achsenparallel).</td>
</tr>
<tr class="even">
<td align="left">Perimeter</td>
<td align="left">Der Umfang.</td>
</tr>
<tr class="odd">
<td align="left">Compactness</td>
<td align="left">Die Kompaktheit.</td>
</tr>
<tr class="even">
<td align="left">Convex Hull</td>
<td align="left">Die konvexe Hülle.</td>
</tr>
<tr class="odd">
<td align="left">Convex Area</td>
<td align="left">Die Flöche der konvexen Hülle.</td>
</tr>
<tr class="even">
<td align="left">Perforation</td>
<td align="left">Die Perforation.</td>
</tr>
<tr class="odd">
<td align="left">Convexity</td>
<td align="left">Die Konvexität.</td>
</tr>
<tr class="even">
<td align="left">Convex Perimeter</td>
<td align="left">Der Umfang der konvexen Hülle.</td>
</tr>
<tr class="odd">
<td align="left">Maximum Feret Diameter</td>
<td align="left">Der maximale Durchmesser der konvexen Hülle.</td>
</tr>
<tr class="even">
<td align="left">Minimum Feret Diameter</td>
<td align="left">Der minimale Durchmesser der konvexen Hülle.</td>
</tr>
<tr class="odd">
<td align="left">Minimum Bounding Circle</td>
<td align="left">Der minimale Umkreis.</td>
</tr>
<tr class="even">
<td align="left">Inner Contour</td>
<td align="left">Die innere Kontur (liegt innerhalb der Region).</td>
</tr>
<tr class="odd">
<td align="left">Inner Contour Perimeter</td>
<td align="left">Die Länge der inneren Kontur.</td>
</tr>
<tr class="even">
<td align="left">Outer Contour</td>
<td align="left">Die äußere Kontur (liegt im Hintergrund).</td>
</tr>
<tr class="odd">
<td align="left">Outer Contour Perimeter</td>
<td align="left">Die Länge der äußeren Kontur.</td>
</tr>
<tr class="even">
<td align="left">Raw Moments</td>
<td align="left">Die regionen-basierten Momente.</td>
</tr>
<tr class="odd">
<td align="left">Normalized Moments</td>
<td align="left">Die regionen-basierten normalisierten Momente.</td>
</tr>
<tr class="even">
<td align="left">Centroid</td>
<td align="left">Der Schwerpunkt, basierend auf der Region.</td>
</tr>
<tr class="odd">
<td align="left">Central Moments</td>
<td align="left">Die regionen-basierten Znetralmomente.</td>
</tr>
<tr class="even">
<td align="left">Equivalent Ellipse</td>
<td align="left">Die äquivalente Ellipse, basierend auf der Region.</td>
</tr>
<tr class="odd">
<td align="left">Equivalent Ellipse Eccentricity</td>
<td align="left">Die Exzentrizität der äquivalenten Ellipse, basierend auf der Region.</td>
</tr>
<tr class="even">
<td align="left">Scale Invariant Moments</td>
<td align="left">Die regionen-basierten größeninvarianten Momente..</td>
</tr>
<tr class="odd">
<td align="left">Hu Moments</td>
<td align="left">Die regionen-basierten Hu Momente.</td>
</tr>
<tr class="even">
<td align="left">Flusser Moments</td>
<td align="left">Die regionen-basierten Flusser Momente.</td>
</tr>
<tr class="odd">
<td align="left">Holes</td>
<td align="left">Die Löcher der Region als Liste von Regionen.</td>
</tr>
<tr class="even">
<td align="left">Number of Holes</td>
<td align="left">Die Anzahl der Löcher der Region.</td>
</tr>
<tr class="odd">
<td align="left">Area of Holes</td>
<td align="left">Die Fläche der Löcher der Region.</td>
</tr>
<tr class="even">
<td align="left">Fiber Length</td>
<td align="left">Die Faser-Länge (für löngliche Objekte).</td>
</tr>
<tr class="odd">
<td align="left">Fiber Width</td>
<td align="left">Die Faser-Dicke (für längliche Objekte).</td>
</tr>
<tr class="even">
<td align="left">Bounding Rectangle</td>
<td align="left">Das umgebende Rechteck der Region. Dieses Rechteck hat die gleiche Orientierung wie die äquivalente Ellipse.</td>
</tr>
<tr class="odd">
<td align="left">Minimum Area Bounding Rectangle</td>
<td align="left">Das umgebende Rechteck der Region. Dieses Rechteck hat die kleinst-mögliche Fläche.</td>
</tr>
<tr class="even">
<td align="left">Minimum Perimeter Bounding Rectangle</td>
<td align="left">Das umgebende Rechteck der Region. Dieses Rechteck hat den kleinst-möglichen Umfang.</td>
</tr>
</tbody>
</table>

Hier ist eine Liste der Pixel-basierten Merkmale:

<table>
<colgroup>
<col width="23%" />
<col width="76%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Merkmal</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Minimum Position</td>
<td align="left">Die Position des Minimums.</td>
</tr>
<tr class="even">
<td align="left">Minimum</td>
<td align="left">Der minimale Pixelwert..</td>
</tr>
<tr class="odd">
<td align="left">Maximum Position</td>
<td align="left">Die Position des Maximums.</td>
</tr>
<tr class="even">
<td align="left">Maximum</td>
<td align="left">Der maximale Pixelwert.</td>
</tr>
<tr class="odd">
<td align="left">Total</td>
<td align="left">Die Summe aller Pixelwerte.</td>
</tr>
<tr class="even">
<td align="left">Mean</td>
<td align="left">Der mittlere Pixelwert.</td>
</tr>
<tr class="odd">
<td align="left">Standard Deviation</td>
<td align="left">Die Standardabweichung der Pixelwerte.</td>
</tr>
<tr class="even">
<td align="left">Skewness</td>
<td align="left">Die Schiefe der Pixelwerte.</td>
</tr>
<tr class="odd">
<td align="left">Channel Minimum</td>
<td align="left">Der Kanal-weise minimale Pixelwert.</td>
</tr>
<tr class="even">
<td align="left">Channel Maximum</td>
<td align="left">Der Kanal-weise maximale Pixelwert.</td>
</tr>
<tr class="odd">
<td align="left">Contrast</td>
<td align="left">Der Kontrast.</td>
</tr>
<tr class="even">
<td align="left">Weber Contrast</td>
<td align="left">Der Kontrast nach der Weber Formel.</td>
</tr>
<tr class="odd">
<td align="left">Michelson Contrast</td>
<td align="left">Der Kontrast nach der Michelson Formel.</td>
</tr>
<tr class="even">
<td align="left">Horizontal Profile</td>
<td align="left">Das horizontale Profil.</td>
</tr>
<tr class="odd">
<td align="left">Vertical Profile</td>
<td align="left">Das vertikale Profil.</td>
</tr>
<tr class="even">
<td align="left">Histogram</td>
<td align="left">Das Histogram.</td>
</tr>
<tr class="odd">
<td align="left">Raw Moments</td>
<td align="left">Die Pixel-basierten Momente.</td>
</tr>
<tr class="even">
<td align="left">Normalized Moments</td>
<td align="left">Die Pixel-basierten normalisierten Momente.</td>
</tr>
<tr class="odd">
<td align="left">Centroid</td>
<td align="left">Der Schwerpunkt, basierend auf den Pixeln.</td>
</tr>
<tr class="even">
<td align="left">Central Moments</td>
<td align="left">Die Pixel-basierten Znetralmomente.</td>
</tr>
<tr class="odd">
<td align="left">Equivalent Ellipse</td>
<td align="left">Die äquivalente Ellipse, basierend auf den Pixeln.</td>
</tr>
<tr class="even">
<td align="left">Equivalent Ellipse Eccentricity</td>
<td align="left">Die Exzentrizität der äquivalenten Ellipse, basierend auf den Pixeln.</td>
</tr>
<tr class="odd">
<td align="left">Scale Invariant Moments</td>
<td align="left">Die Pixel-basierten größeninvarianten Momente..</td>
</tr>
<tr class="even">
<td align="left">Hu Moments</td>
<td align="left">Die Pixel-basierten Hu Momente.</td>
</tr>
<tr class="odd">
<td align="left">Flusser Moments</td>
<td align="left">Die Pixel-basierten Flusser Momente.</td>
</tr>
</tbody>
</table>

Regionsmerkmale
===============

Das einfachste Merkmal das man berechnen kann - nicht ein
Regionenmerkmal im engsten Sinne - ist die Anzahl der Objekte in einem
Bild.

![Berechnung der Anzahl der Objekte in einem
Bild.](images/blob_count.png)

Das zweit-einfachste Merkmal ist die Regionen-Fläche, und die ist
wirkleich ein Regionenmerkmal im engeren Sinn.

Fläche/Area
-----------

Die Fläche einer Region ist eine ganze Zahl und gibt die Anzahl der
Pixel eines Objekts an. Sie wird effizient berechnet, indem die Länge
aller Sehnen der Region addiert wird.

![Berechnung der Anzahl der Objekte in einem
Bild.](images/blob_area.png)

Die Objektfläche wird auch oft verwendet, um kleine oder große Objekte
auszufiltern.

Links/Left
----------

Berechnet die minimale x Koordinate (inklusiv) der Region.

![Berechnung der linken Position.](images/blob_left.png)

Regionen mit einem Left Wert von 0 (oder \< 0) berühren (oder schneiden)
den linken Bildrand. Dieses Merkmal wird manchmal verwendet, um Objekte
auszufiltern, die den linken Rand berühren.

![Darstellung der linken
Position.](images/d2d_blob_analysis_left_graphics.png)

Da es sich um eine ganzzahlige Koordinate handelt, liegt sie gedanklich
in der Mitte des am weitesten links liegenden Pixels.

Rechts/Right
------------

Berechnet die maximale x Koordinate (exklusiv) der Region.

![Berechnung der rechten Position.](images/blob_right.png)

Regionen mit einem Right Wert gleich (oder größer als) der Bildbreite
berühren (oder schneiden) den rechten Bildrand. Dieses Merkmal wird
manchmal verwendet, um Objekte auszufiltern, die den rechten Rand
berühren.

![Darstellung der rechten
Position.](images/d2d_blob_analysis_right_graphics.png)

Da es sich um eine ganzzahlige Koordinate handelt, liegt sie gedanklich
in der Mitte des Hintergrundpixels, das rechts neben dem am weitesten
rechts liegenden Pixel liegt.

Oben/Top
--------

Berechnet die minimale y Koordinate (inklusiv) der Region.

![Berechnung der obersten Position.](images/blob_top.png)

Regionen mit einem Top Wert von 0 (oder \< 0) berühren (oder schneiden)
den oberen Bildrand. Dieses Merkmal wird manchmal verwendet, um Objekte
auszufiltern, die den oberen Rand berühren.

![Darstellung der oberen
Position.](images/d2d_blob_analysis_top_graphics.png)

Da es sich um eine ganzzahlige Koordinate handelt, liegt sie gedanklich
in der Mitte des am weitesten oben liegenden Pixels.

Unten/Bottom
------------

Berechnet die maximale y Koordinate (exklusiv) der Region.

![Berechnung der untersten Position.](images/blob_bottom.png)

Regionen mit einem Bottom Wert gleich (oder größer als) der Bildhöhe
berühren (oder schneiden) den unteren Bildrand. Dieses Merkmal wird
manchmal verwendet, um Objekte auszufiltern, die den unteren Rand
berühren.

![Darstellung der unteren
Position.](images/d2d_blob_analysis_bottom_graphics.png)

Da es sich um eine ganzzahlige Koordinate handelt, liegt sie gedanklich
in der Mitte des Hintergrundpixels, das unterhalb neben dem am weitesten
unten liegenden Pixel liegt.

Width
-----

This calculates the axis-parallel width of the region.

    blob_features b = objects[0]; 
    int width = b.get_width();

The width value of the blob in picture blob.png is 80.

Height
------

This calculates the axis-parallel height of the region.

    blob_features b = objects[0]; 
    int height = b.get_height();

The height value of the blob in picture blob.png is 59.

Aspect Ratio
------------

This calculates the axis-parallel aspect ratio of the region. The aspect
ratio is calculated according to the following formula:

$$r = \\frac{w}{h}$$

where *w* is the width and *h* is the height.

Another way to calculate this feature is with the blob\_features class:

    blob_features b = objects[0]; 
    double aspect_ratio = b. get_aspect_ratio ();

The aspect ratio value of the blob in picture blob.png is 1.35593.

Bounds
------

This calculates the bounds of the region.

Integer coordinates are in the middle of a pixel. Pixel boundaries are
offset by half a pixel horizontally and vertically, and so are the
region bounds.

    box<double> bounds = *objects[0].get_bounds();

Another way to calculate this feature is with the blob\_features class:

    blob_features b = objects[0]; 
    box<double> bounds = *b.get_bounds();

The bounds encompass the whole region tightly, thus the bounds
coordinates are returned in floating point values.

The bounds value of the blob in picture blob.png is a box with the
top-left coordinate at (8.5, 10.5) and a size of (80, 59).

You may have noted the indirection in the bounds assignment above. This
is because the `blob_features` class calculates and caches the
`box<double>` on demand only. It therefore only allocates space when
needed and returns a `shared_ptr<box<double>>`, which is why you need
the indirection in order to get at the bounds value. All features that
occupy more space than a single value, are handled via a `shared_ptr<>`.

Perimeter
---------

The perimeter of a region specifies the length of the object outline.

    double perimeter = objects[0].get_perimeter(); 

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0]; 
    double perimeter = b.get_perimeter();

![](images/d2d_blob_analysis_perimeter_graphics.png)

The perimeter of the blob in picture blob.png is 660.

Compactness
-----------

This calculates the compactness of the region.

    blob_features b = objects[0]; 
    double compactness = b.get_compactness();

The compactness *c* is calculated from the region area *a* and the
region perimeter *p* according to this formula:

$$c = \\frac {p^{2}} {4 \\pi a}$$

The compactness value of the blob in picture blob.png is 41.5.

Convex Hull
-----------

This calculates a polyline that constitutes the convex hull of the
region.

    polyline<double> hull = objects[0].to_point_list().get_convex_hull(); 

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0]; 
    polyline<double> hull = *b.get_convex_hull();

![](images/d2d_blob_analysis_convex_hull_graphics.png)

The convex hull can be used to calculate further features, such as the
area of the convex hull, the perimeter of the convex hull, as well as
the minimum and maximum width of the convex hull. The minimum and the
maximum width of the convex hull are calculated using a rotating caliper
algorithm and are often called the Feret diameters.

Convex Area
-----------

This calculates the area enclosed by the convex hull.

    double area = objects[0].to_point_list().get_convex_hull().get_area(); 

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0]; 
    double area = b.get_convex_area();

The convex area is always the same or bigger than the area of the
original region. For the example image one can see right away that it is
certainly bigger.

The convex area value of the blob in picture blob.png is 2325.5.

Perforation
-----------

This calculates the perforation of the region.

    blob_features b = objects[0]; 
    double perforation = b.get_perforation();

The perforation *p* is calculated from the region area *a* and the area
of the holes *a*<sub>*h*</sub> according to this formula:

$$p = \\frac { { a }\_{ h } }{ a }$$

The perforation is zero for a region that has no holes, and it increases
the more holes the region has. The perforation is scale invariant.

The perforation value of the blob in picture blob.png is 1.78.

Convexity
---------

This calculates the convexity of the region.

    blob_features b = objects[0]; 
    double convexity = b.get_convexity();

The convexity *c* is calculated from the region area *a* and the convex
area *a*<sub>*c*</sub> according to this formula:

$$c = \\frac { a }{ { a }\_{ c } }$$

The convexity is 1 for a region that already is convex. It is smaller
than 1 for non-convex regions. The convexity is scale invariant.

The convexity value of the blob in picture blob.png is 0.36.

Convex Perimeter
----------------

This calculates the perimeter of the convex hull polygon.

    double perimeter = objects[0].to_point_list().get_convex_hull().get_perimeter(); 

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0]; 
    double perimeter = b.get_convex_perimeter();

The convex perimeter value of the blob in picture blob.png is 205.14.

Minimum Feret Diameter
----------------------

This calculates the minimum feret diameter of the convex hull polygon.
The minimum feret diameter is calculated using a rotating calipers
algorithm.

    double min_feret = objects[0].to_point_list().get_convex_hull().get_minimum_width(); 

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0]; 
    double min_feret = b.get_minimum_feret();

![](images/minimum_feret_diameter.png)

The minimum feret diameter value of the blob in picture blob.png is
31.76.

Maximum Feret Diameter
----------------------

This calculates the maximum feret diameter of the convex hull polygon.
The maximum feret diameter is calculated using a rotating calipers
algorithm.

    double max_feret = objects[0].to_point_list().get_convex_hull().get_maximum_width();

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0];
    double max_feret = b.get_maximum_feret();

![](images/maximum_feret_diameter.png)

The maximum feret diameter value of the blob in picture blob.png is
82.71.

Minimum Bounding Circle
-----------------------

This calculates the smallest bounding circle of the region.

    blob_features b = objects[0];
    circle disc = *b.get_minimum_bounding_circle();

![](images/d2d_blob_analysis_minimum_bounding_circle_graphics.png)

Inner Contour
-------------

This calculates a contour of the object that is a counter-clockwise
chain code of object boundary pixels. The inner contour exclusively lies
on object pixels and is an 8-connected contour.

    chain_code contour = objects[0].get_inner_contour();

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0];
    chain_code contour = *b.get_inner_contour();

![](images/blob_inner_contour.png)

Inner Contour Perimeter
-----------------------

This calculates the object perimeter using the inner contour.

    double perimeter = objects[0].get_inner_contour().get_contour_length();

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0];
    double perimeter = b.get_inner_contour_perimeter();

The inner contour is 8-connected. The contour length sums the contour
segments for each of the chain code directions. Horizontal and vertical
directions count as 1, diagonal directions count as $\\sqrt{2}$.

Outer Contour
-------------

This calculates a contour of the object that is a clockwise chain code
of background pixels just outside the object. The outer contour
exclusively lies on background pixels and is a 4-connected contour.

    chain_code contour = objects[0].get_outer_contour();

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0];
    chain_code contour = *b.get_outer_contour();

![](images/blob_outer_contour.png)

Outer Contour Perimeter
-----------------------

This calculates the object perimeter using the outer contour.

    double perimeter = objects[0].get_outer_contour().get_contour_length();

Another way to calculate this feature is with the `blob_features` class:

    blob_features b = objects[0];
    double perimeter = b.get_outer_contour_perimeter();

The outer contour is 4-connected. The contour length sums the contour
segments for each of the chain code directions. Horizontal and vertical
directions count as 1, diagonal directions are not used.

Region-based Moments
--------------------

This calculates the region-based raw moments. There is a specific
chapter about moments that you can consult, if you want to learn more
about moments and how they are implemented. Here, in the context of blob
analysis, they are documented only briefly.

    moments raw = objects[0].get_raw_moments();

Another way to calculate this feature is with the blob\_features class:

    blob_features b = objects[0];
    moments raw = *b.get_region_raw_moments ();

The region-based raw moments are calculated up to the third order,
according to the following formula:

*M*<sub>*p**q*</sub> = ∑<sub>*x*</sub>∑<sub>*y*</sub>*x*<sup>*p*</sup>*y*<sup>*q*</sup>

The moments class has the properties m00, m10, m01, m20, m11, m02, m30,
m21, m12 and m03 to read the respective moment.

Region-based Normalized Moments
-------------------------------

This calculates the region-based normalized moments.

    blob_features b = objects[0];
    normalized_moments nm = *b.get_normalized_moments ();

They are calculated up to the third order, according to the following
formula:

$${N}\_{{pq}}={\\frac{{M}\_{{pq}}}{{M}\_{{00}}}}$$

Normalized moments are invariant with respect to scaling, but are not
invariant with respect to translation and rotation.

You can use the properties n10, n01, n20, n11, n02, n30, n21, n12 and
n03 to read the respective normalized moments.

Region-based Centroid
---------------------

This calculates the region-based object centroid. The centroid is a
property of the normalized moments.

    blob_features b = objects[0]; 
    point<double> centroid = *b.get_region_centroid();

The centroid is the center of gravity and calculated according to the
following formula:

$$\\left( \\begin{matrix} x \\\\ y \\end{matrix} \\right) =\\left( \\begin{matrix} \\frac { { N }\_{ 10 } }{ { N }\_{ 00 } }  \\\\ \\frac { { N }\_{ 01 } }{ { N }\_{ 00 } }  \\end{matrix} \\right)$$

![](images/d2d_blob_analysis_region_centroid_graphics.png)

The centroid value of the blob in picture blob.png is (47.02, 40.61).

Region-based Central Moments
----------------------------

This calculates the region-based central moments.

    blob_features b = objects[0]; 
    central_moments cm = *b.get_region_central_moments ();

They are calculated up to the third order according to the following
formula:

$${ \\mu  }\_{ pq }=\\frac { 1 }{ { M }\_{ 00 } } \\sum \_{ x }^{  }{ \\sum \_{ y }^{  }{ { (x-\\overline { x } ) }^{ p }{ (y-\\overline { y } ) }^{ q } }  }$$

Central moments are invariant with respect to translation, but are not
invariant with respect to rotation. You can use the properties mu20,
mu11, mu02, mu30, mu21, mu12 and mu03 to read the respective central
moments.

Region-based Equivalent Ellipse
-------------------------------

This calculates the region\_based equivalent ellipse of the object. The
equivalent ellipse is built by combining properties from the normalized
and the central moments.

    blob_features b = objects[0]; 
    ellipse equivalent_ellipse = b.get_region_equivalent_ellipse();

![](images/d2d_blob_analysis_region_equivalent_ellipse_graphics.png)

Region-based Scale Invariant Moments
------------------------------------

This calculates the region-based scale-invariant moments.

    blob_features b = objects[0]; 
    scale_invariant_moments sim = *b.get_scale_invariant_moments();

They are calculated up to the third order according to the following
formula:

$${\\eta}\_{{pq}}={\\frac{{\\mu}\_{{pq}}}{{\\mu}\_{{00}}^{\\frac{p+q}{2}+1}}}$$

Scale-invariant moments are invariant with respect to translation and
scaling, but are not invariant with respect to rotation.

You can use the properties eta20, eta11, eta02, eta30, eta21, eta12 and
eta03 to read the respective scale invariant moments.

Region-based Hu Moments
-----------------------

This calculates the region-based Hu moments. The formulas are described
in the chapter about moments.

    blob_features b = objects[0]; 
    hu_moments hm = *b.get_hu_moments();

Hu's moments are invariant with respect to translation, scaling and
rotation.

The formulas for Hu's moments are:

$$\\begin{aligned}
    {\\phi}\_{{1}} & = & {{\\eta}\_{{20}}+{\\eta}\_{{02}}} \\\\
    {\\phi}\_{{2}} & = & {({\\eta}\_{{20}}-{\\eta}\_{{02}})^2+4{\\eta}\_{{11}}^2} \\\\
    {\\phi}\_{{3}} & = & {({\\eta}\_{{30}}-3{\\eta}\_{{12}})^2+(3{\\eta}\_{{21}}-{\\eta}\_{{03}})^2} \\\\
    {\\phi}\_{{4}} & = & {({\\eta}\_{{30}}+{\\eta}\_{{12}})^2+({\\eta}\_{{21}}+{\\eta}\_{{03}})^2} \\\\
    {\\phi}\_{{5}} & = & {({\\eta}\_{{30}}-3{\\eta}\_{{12}})({\\eta}\_{{30}}+{\\eta}\_{{12}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-3({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{+(3{\\eta}\_{{21}}-{\\eta}\_{{03}})({\\eta}\_{{21}}+{\\eta}\_{{03}})\\left[3({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\\\
    {\\phi}\_{{6}} & = & {({\\eta}\_{{20}}-{\\eta}\_{{02}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{+4{\\eta}\_{{11}}({\\eta}\_{{30}}+{\\eta}\_{{12}})({\\eta}\_{{21}}+{\\eta}\_{{03}})} \\\\
    {\\phi}\_{{7}} & = & {(3{\\eta}\_{{21}}-3{\\eta}\_{{03}})({\\eta}\_{{30}}+{\\eta}\_{{12}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-3({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{-({\\eta}\_{{30}}-3{\\eta}\_{{12}})({\\eta}\_{{21}}+{\\eta}\_{{03}})\\left[3({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]}\\end{aligned}$$

You can use the properties phi1, phi2, phi3, phi4, phi5, phi6 and phi7
to read the respective Hu moment.

Region-based Flusser Moments
----------------------------

This calculates the region-based Flusser moments. The formulas are
described in the chapter about moments.

    blob_features b = objects[0]; 
    flusser_moments fm = *b.get_flusser_moments();

Flusser's moments are invariant with respect to affine transformations.

The formulas for Flusser's moments are:

$$\\begin{aligned}
    {I}\_{{1}} & = & {\\frac{{\\mu}\_{{20}}{\\mu}\_{{02}}-{\\mu}\_{{11}}^2}{{\\mu}\_{{00}}^4}} \\\\
    {I}\_{{2}} & = & {\\frac{{\\mu}\_{{30}}^2{\\mu}\_{{03}}^2-6{\\mu}\_{{30}}{\\mu}\_{{21}}{\\mu}\_{{12}}{\\mu}\_{{03}}+4{\\mu}\_{{30}}{\\mu}\_{{12}}^3+4{\\mu}\_{{21}}^3{\\mu}\_{{03}}-3{\\mu}\_{{21}}^2{\\mu}\_{{12}}^2}{{\\mu}\_{{00}}^10}} \\\\
    {I}\_{{3}} & = & {\\frac{{\\mu}\_{{20}}({\\mu}\_{{21}}{\\mu}\_{{03}}-{\\mu}\_{{12}}^2)-{\\mu}\_{{11}}({\\mu}\_{{30}}{\\mu}\_{{03}}-{\\mu}\_{{21}}{\\mu}\_{{12}}))}{{\\mu}\_{{00}}^7}} \\nonumber \\\\
    & & {+\\frac{{\\mu}\_{{02}}({\\mu}\_{{30}}{\\mu}\_{{12}}-{\\mu}\_{{21}}^2)}{{\\mu}\_{{00}}^7}} \\\\
    {I}\_{{4}} & = & {\\frac{{\\mu}\_{{20}}^3{\\mu}\_{{03}}^2-6{\\mu}\_{{20}}^2{\\mu}\_{{11}}{\\mu}\_{{12}}({\\mu}\_{{03}}-6{\\mu}\_{{20}}^2{\\mu}\_{{02}}{\\mu}\_{{21}}){\\mu}\_{{03}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{9{\\mu}\_{{20}}^2{\\mu}\_{{02}}{\\mu}\_{{12}}^2+12{\\mu}\_{{20}}{\\mu}\_{{11}}^2{\\mu}\_{{21}}{\\mu}\_{{03}}+6{\\mu}\_{{20}}{\\mu}\_{{11}}{\\mu}\_{{02}}{\\mu}\_{{30}}{\\mu}\_{{03}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {-\\frac{18{\\mu}\_{{20}}{\\mu}\_{{11}}{\\mu}\_{{02}}{\\mu}\_{{21}}{\\mu}\_{{12}}+8{\\mu}\_{{11}}^3{\\mu}\_{{30}}{\\mu}\_{{03}}+6{\\mu}\_{{20}}{\\mu}\_{{02}}^2{\\mu}\_{{30}}{\\mu}\_{{12}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{9{\\mu}\_{{20}}{\\mu}\_{{02}}^2{\\mu}\_{{21}}^2+12{\\mu}\_{{11}}^2{\\mu}\_{{02}}{\\mu}\_{{30}}{\\mu}\_{{12}}-6{\\mu}\_{{11}}{\\mu}\_{{02}}^2{\\mu}\_{{30}}{\\mu}\_{{21}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{{\\mu}\_{{02}}^3{\\mu}\_{{30}}^2}{{\\mu}\_{{00}}^{11}}}\\end{aligned}$$

You can use the properties i1, i2, i3, and i4 to read the respective
flusser moments.

Holes
-----

This calculates the holes of the region.

    blob_features b = objects[0]; 
    region_list rl = *b.get_holes();

The holes are returned as a region\_list of connected components. Using
this region\_list, you can do another blob analysis on the holes, if
necessary. If you need just the number of holes or the area of the
holes, you can use the Number of Holes or Area of Holes features.

Number of Holes
---------------

This calculates the number of holes of the region.

    blob_features b = objects[0]; 
    int n = *b.get_number_of_holes();

The number of holes value of the blob in picture blob.png is 5.

Area of Holes
-------------

This calculates the total area of the holes of the region.

    blob_features b = objects[0]; 
    size_t area = *b.get_area_of_holes();

The area of holes value of the blob in picture blob.png is 244.

Fiber Length
------------

This calculates an approximation of a fiber length. This measurement is
only valid for elongated objects. The fiber length is approximated as
half the perimeter.

    blob_features b = objects[0]; 
    double length = *b.get_fiber_length();

Fiber Width
-----------

This calculates an approximation of a fiber width. The fiber width is
approximated as twice the maximum value of the distance transform.

    blob_features b = objects[0]; 
    double width = *b.get_fiber_width();

Bounding Rectangle
------------------

This calculates a bounding rectangle of the region.

    blob_features b = objects[0]; 
    rectangle r = *b.get_bounding_rectangle();

![](images/d2d_blob_analysis_bounding_rectangle_graphics.png)

The rectangle has the same orientation as the equivalent ellipse.

Minimum Area Bounding Rectangle
-------------------------------

This calculates a bounding rectangle of the region.

    blob_features b = objects[0]; 
    rectangle r = *b.get_minimum_area_bounding_rectangle();

![](images/d2d_blob_analysis_minimum_area_bounding_rectangle_graphics.png)

The result is the rectangle with the smallest area bounding the region.

Minimum Perimeter Bounding Rectangle
------------------------------------

This calculates a bounding rectangle of the region.

    blob_features b = objects[0]; 
    rectangle r = *b.get_minimum_perimeter_bounding_rectangle();

The result is the rectangle with the smallest perimeter bounding the
region.

![](images/d2d_blob_analysis_minimum_perimeter_bounding_rectangle_graphics.png)

Pixel Based Features
====================

In contrast to region-based features, pixel-based features use the pixel
values in addition to the region definition. Pixel-based features are
necessary since some features, such as the minimum pixel value, cannot
be calculated given the region alone. However, the calculation of
pixel-based features is slower than the calculation of region-based
features.

Minimum Position
----------------

This calculates the minimum position value.

    blob_features b(gray.get_view(), objects[0]); 
    point_3d<int> minimum = *b.get_minimum_position();

When calculating the minimum position for color images, the color is
treated as a vector and the minimum is determined by comparing the
Euclidean vector length.

The minimum position value of the blob in picture pair
`blob.png/blobmono.png` is (x = 38 y = 54 z = 0).

The minimum position value of the blob in picture pair
`blob.png/blobcolor.png` is (x = 25 y = 68 z = 0).

Minimum
-------

This calculates the minimum value.

blob\_features b(gray.get\_view(), objects[0]); pixel\_type minimum =
b.get\_minimum();

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `unsigned char`, for `image<rgb<double>>`
it is `rgb<double>`.

When calculating the minimum for color images, the color is treated as a
vector and the minimum is determined by comparing the Euclidean vector
length.

There is also the Channel Minimum, which does determine the minimum
channel by channel for color images.

The minimum value of the blob in picture pair `blob.png/blobmono.png` is
29.

The minimum value of the blob in picture pair `blob.png/blobcolor.png`
is (red = 32 green = 59 blue = 37).

Maximum Position
----------------

This calculates the maximum position value.

    blob_features b(gray.get_view(), objects[0]); 
    point_3d<int> maximum = *b.get_maximum_position();

When calculating the minimum position for color images, the color is
treated as a vector and the minimum is determined by comparing the
Euclidean vector length.

The maximum position value of the blob in picture pair
`blob.png/blobmono.png` is (x = 80 y = 25 z = 0).

The maximum position value of the blob in picture pair
`blob.png/blobcolor.png` is (x = 67 y = 15 z = 0).

Maximum
-------

This calculates the maximum value.

    blob_features b(gray.get_view(), objects[0]); 
    pixel_type maximum = b.get_maximum(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `unsigned char`, for `image<rgb<double>>`
it is `rgb<double>`.

When calculating the maximum for color images, the color is treated as a
vector and the maximum is determined by comparing the Euclidean vector
length.

There is also the Channel Maximum, which does determine the maximum
channel by channel for color images.

The maximum value of the blob in picture pair `blob.png/blobmono.png` is
207. The maximum value of the blob in picture pair
`blob.png/blobcolor.png` is (red = 240 green = 218 blue = 203).

Total
-----

This calculates the total value.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type total = b.get_total(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The total is the sum of all pixel values of the object:

*T* = ∑<sub></sub>*I*

The total value of the blob in picture pair `blob.png/blobmono.png` is
91963.

The total value of the blob in picture pair `blob.png/blobcolor.png` is
(red = 107558 green = 107465 blue = 103216).

Mean
----

This calculates the mean value.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type mean = b.get_mean(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The mean is calculated according to the following formula:

$$M = \\frac { T }{ n }$$

where *T* is the total value and *n* is the number of pixels (i.e. the
area) of the blob.

The mean value of the blob in picture `pair blob.png/blobmono.png` is
110.135. The mean value of the blob in picture
`pair blob.png/blobcolor.png` is (red = 128.812 green = 128.701 blue =
123.612).

Standard Deviation
------------------

This calculates the standard deviation value. Standard deviation is a
measure of how wide the distribution of values is.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type stddev = b.get_standard_deviation(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The standard deviation is calculated according to the following formula:

$$\\sigma = \\sqrt {\\frac { 1 }{ n } \\sum {}{(I-E)^2}}$$

The standard deviation value of the blob in picture pair
`blob.png/blobmono.png` is 49.447.

The standard deviation value of the blob in picture pair
`blob.png/blobcolor.png` is (red = 75.3174 green = 71.9041 blue =
42.301).

Skewness
--------

This calculates the skewness value. Skewness can be understood as a
measure of asymmetry in the distribution, i.e. how much it deviates from
a gauss-shaped curve.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type mean = b.get_mean(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The skewness is calculated according to the following formula:

$$\\sigma = \\sqrt[3]{\\frac { 1 }{ n } \\sum {}{(I-E)^3}}$$

The skewness value of the blob in picture pair `blob.png/blobmono.png`
is 30.6915.

The skewness value of the blob in picture pair `blob.png/blobcolor.png`
is (red = -20.3929 green = 21.2389 blue = 20.5604).

Channel Minimum
---------------

This calculates the channel-wise minimum value.

    blob_features b(gray.get_view(), objects[0]); 
    pixel_type minimum = b.get_channel_minimum(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `unsigned char`, for `image<rgb<double>>`
it is `rgb<double>`.

When calculating the channel minimum for color images, the color is
treated as three scalars, and for each one a minimum value is
determined.

There is also the Minimum, which does determine the minimum by comparing
vector length for color images.

The channel minimum value of the blob in picture pair
`blob.png/blobmono.png` is 29.

The channel minimum value of the blob in picture pair
`blob.png/blobcolor.png` is (red = 32 green = 59 blue = 37).

Channel Maximum
---------------

This calculates the channel-wise maximum value.

    blob_features b(gray.get_view(), objects[0]); 
    pixel_type maximum = b.get_channel_maximum(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `unsigned char`, for `image<rgb<double>>`
it is `rgb<double>`.

When calculating the channel maximum for color images, the color is
treated as three scalars, and for each one a maximum value is
determined.

There is also the Maximum, which does determine the maximum by comparing
vector length for color images.

The channel maximum value of the blob in picture pair
`blob.png/blobmono.png` is 207.

The channel maximum value of the blob in picture pair
`blob.png/blobcolor.png` is (red = 255 green = 219 blue = 220).

Contrast
--------

This calculates the channel-wise contrast.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type contrast = b.get_contrast(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The contrast is calculated according to the following formula:

$$c=\\frac {max}{min + 1}$$

Weber Contrast
--------------

This calculates the channel-wise contrast acoording to Weber's formula.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type contrast = b.get_weber_contrast(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The contrast is calculated according to the following formula:

$$c=\\frac {max - min}{min + 1}$$

Michelson Contrast
------------------

This calculates the channel-wise contrast acoording to Michelson's
formula.

    blob_features b(gray.get_view(), objects[0]); 
    float_pixel_type contrast = b.get_mihcelson_contrast(); 

The resulting type depends on the image type, i.e. for
`image<unsigned char>` it is `double`, for `image<rgb<double>>` it
is`rgb<double>`.

The contrast is calculated according to the following formula:

$$c=\\frac {max - min}{max + min + 1}$$

Horizontal Profile
------------------

This calculates the horizontal profile.

    blob_features b(grey.get_view(), objects[0]); 
    profile<pixel_type> p = *b.get_horizontal_profile();

Vertical Profile
----------------

This calculates the vertical profile.

    blob_features b(grey.get_view(), objects[0]); 
    profile<pixel_type> p = *b.get_vertical_profile();

Histogram
---------

This calculates the histogram.

    blob_features b(grey.get_view(), objects[0]); 
    histogram_type p = *b.get_histogram();

Pixel-based Moments
-------------------

This calculates the pixel-based raw moments. There is a specific chapter
about moments that you can consult, if you want to learn more about
moments and how they are implemented. Here, in the context of blob
analysis, they are documented only briefly.

    blob_features b(grey.get_view(), objects[0]); 
    moments raw = *b.get_pixel_raw_moments();

The pixel-based raw moments are calculated up to the third order,
according to the following formula:

*M*<sub>*p**q*</sub> = ∑<sub>*x*</sub>∑<sub>*y*</sub>*x*<sup>*p*</sup>*y*<sup>*q*</sup>*I*<sub>*x**y*</sub>

Pixel based moments are similar to region-based moments, but they use
pixel values in addition. The brighter a pixel is the more weight it has
in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated. The moments class has the
properties m00, m10, m01, m20, m11, m02, m30, m21, m12 and m03 to read
the respective moment.

Pixel-based Normalized Moments
------------------------------

This calculates the pixel-based normalized moments.

    blob_features b(grey.get_view(), objects[0]); 
    normalized_moments nm = *b.get_pixel_normalized_moments();

They are calculated up to the third order, according to the following
formula:

$${N}\_{{pq}}={\\frac{{M}\_{{pq}}}{{M}\_{{00}}}}$$

Pixel based moments are similar to region-based moments, but they use
pixel values in addition. The brighter a pixel is the more weight it has
in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated. You can use the properties
n10, n01, n20, n11, n02, n30, n21, n12 and n03 to read the respective
normalized moments.

Pixel-based Centroid
--------------------

This calculates the pixel-based object centroid. The centroid is a
property of the normalized moments.

    blob_features b(grey.get_view(), objects[0]); 
    point<double> centroid = b.get_pixel_centroid();

The centroid is the center of gravity and calculated according to the
following formula:

$$\\left( \\begin{matrix} x \\\\ y \\end{matrix} \\right) =\\left( \\begin{matrix} \\frac { { N }\_{ 10 } }{ { N }\_{ 00 } }  \\\\ \\frac { { N }\_{ 01 } }{ { N }\_{ 00 } }  \\end{matrix} \\right)$$

![](images/d2d_blob_analysis_pixel_centroid_mono_graphics.png)

Pixel based centroids are similar to region-based centroids, but they
use pixel values in addition. The brighter a pixel is the more weight it
has in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated.

Pixel-based Central Moments
---------------------------

This calculates the pixel-based central moments.

    blob_features b(grey.get_view(), objects[0]); 
    central_moments cm = *b.get_pixel_central_moments();

They are calculated up to the third order according to the following
formula:

$${ \\mu  }\_{ pq }=\\frac { 1 }{ { M }\_{ 00 } } \\sum \_{ x }^{  }{ \\sum \_{ y }^{  }{ { (x-\\overline { x } ) }^{ p }{ (y-\\overline { y } ) }^{ q }{ I }\_{ xy } }  }$$

Central moments are invariant with respect to translation, but are not
invariant with respect to rotation. Pixel based moments are similar to
region-based moments, but they use pixel values in addition. The
brighter a pixel is the more weight it has in the calculation of the
moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated. You can use the properties
mu20, mu11, mu02, mu30, mu21, mu12 and mu03 to read the respective
central moments.

Pixel-based Equivalent Ellipse
------------------------------

This calculates the pixel-based equivalent ellipse of the object. The
equivalent ellipse is built by combining properties from the normalized
and the central moments.

    blob_features b(grey.get_view(), objects[0]); 
    ellipse equivalent_ellipse = b.get_pixel_equivalent_ellipse();

Pixel based equivalent ellipses are similar to region-based equivalent
ellipses, but they use pixel values in addition. The brighter a pixel is
the more weight it has in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated.

Pixel-based Scale Invariant Moments
-----------------------------------

This calculates the pixel-based scale-invariant moments.

    blob_features b(grey.get_view(), objects[0]); 
    scale_invariant_moments sim = *b.get_pixel_scale_invariant_moments();

They are calculated up to the third order, according to the following
formula:

$${\\eta}\_{{pq}}={\\frac{{\\mu}\_{{pq}}}{{\\mu}\_{{00}}^{\\frac{p+q}{2}+1}}}$$

Scale-invariant moments are invariant with respect to translation and
scaling, but are not invariant with respect to rotation.

Pixel based moments are similar to region-based moments, but they use
pixel values in addition. The brighter a pixel is the more weight it has
in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated. You can use the properties
eta20, eta11, eta02, eta30, eta21, eta12 and eta03 to read the
respective scale invariant moments.

Pixel-based Hu Moments
----------------------

This calculates the pixel-based Hu moments.

    blob_features b(grey.get_view(), objects[0]); 
    hu_moments hm = *b.get_pixel_hu_moments();

The formulas for Hu's moments are:

$$\\begin{aligned}
    {\\phi}\_{{1}} & = & {{\\eta}\_{{20}}+{\\eta}\_{{02}}} \\\\
    {\\phi}\_{{2}} & = & {({\\eta}\_{{20}}-{\\eta}\_{{02}})^2+4{\\eta}\_{{11}}^2} \\\\
    {\\phi}\_{{3}} & = & {({\\eta}\_{{30}}-3{\\eta}\_{{12}})^2+(3{\\eta}\_{{21}}-{\\eta}\_{{03}})^2} \\\\
    {\\phi}\_{{4}} & = & {({\\eta}\_{{30}}+{\\eta}\_{{12}})^2+({\\eta}\_{{21}}+{\\eta}\_{{03}})^2} \\\\
    {\\phi}\_{{5}} & = & {({\\eta}\_{{30}}-3{\\eta}\_{{12}})({\\eta}\_{{30}}+{\\eta}\_{{12}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-3({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{+(3{\\eta}\_{{21}}-{\\eta}\_{{03}})({\\eta}\_{{21}}+{\\eta}\_{{03}})\\left[3({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\\\
    {\\phi}\_{{6}} & = & {({\\eta}\_{{20}}-{\\eta}\_{{02}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{+4{\\eta}\_{{11}}({\\eta}\_{{30}}+{\\eta}\_{{12}})({\\eta}\_{{21}}+{\\eta}\_{{03}})} \\\\
    {\\phi}\_{{7}} & = & {(3{\\eta}\_{{21}}-3{\\eta}\_{{03}})({\\eta}\_{{30}}+{\\eta}\_{{12}})\\left[({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-3({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]} \\nonumber \\\\
    & & {}{-({\\eta}\_{{30}}-3{\\eta}\_{{12}})({\\eta}\_{{21}}+{\\eta}\_{{03}})\\left[3({\\eta}\_{{30}}+{\\eta}\_{{12}})^2-({\\eta}\_{{21}}+{\\eta}\_{{03}})^2\\right]}\\end{aligned}$$

Hu's moments are invariant with respect to translation, scaling and
rotation.

Pixel based moments are similar to region-based moments, but they use
pixel values in addition. The brighter a pixel is the more weight it has
in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated. You can use the properties
phi1, phi2, phi3, phi4, phi5, phi6 and phi7 to read the respective Hu
moment.

Pixel-based Flusser Moments
---------------------------

This calculates the pixel-based Flusser moments.

    blob_features b(grey.get_view(), objects[0]); 
    flusser_moments fm = *b.get_pixel_flusser_moments();

The formulas for Flusser's moments are:

$$\\begin{aligned}
    {I}\_{{1}} & = & {\\frac{{\\mu}\_{{20}}{\\mu}\_{{02}}-{\\mu}\_{{11}}^2}{{\\mu}\_{{00}}^4}} \\\\
    {I}\_{{2}} & = & {\\frac{{\\mu}\_{{30}}^2{\\mu}\_{{03}}^2-6{\\mu}\_{{30}}{\\mu}\_{{21}}{\\mu}\_{{12}}{\\mu}\_{{03}}+4{\\mu}\_{{30}}{\\mu}\_{{12}}^3+4{\\mu}\_{{21}}^3{\\mu}\_{{03}}-3{\\mu}\_{{21}}^2{\\mu}\_{{12}}^2}{{\\mu}\_{{00}}^10}} \\\\
    {I}\_{{3}} & = & {\\frac{{\\mu}\_{{20}}({\\mu}\_{{21}}{\\mu}\_{{03}}-{\\mu}\_{{12}}^2)-{\\mu}\_{{11}}({\\mu}\_{{30}}{\\mu}\_{{03}}-{\\mu}\_{{21}}{\\mu}\_{{12}}))}{{\\mu}\_{{00}}^7}} \\nonumber \\\\
    & & {+\\frac{{\\mu}\_{{02}}({\\mu}\_{{30}}{\\mu}\_{{12}}-{\\mu}\_{{21}}^2)}{{\\mu}\_{{00}}^7}} \\\\
    {I}\_{{4}} & = & {\\frac{{\\mu}\_{{20}}^3{\\mu}\_{{03}}^2-6{\\mu}\_{{20}}^2{\\mu}\_{{11}}{\\mu}\_{{12}}({\\mu}\_{{03}}-6{\\mu}\_{{20}}^2{\\mu}\_{{02}}{\\mu}\_{{21}}){\\mu}\_{{03}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{9{\\mu}\_{{20}}^2{\\mu}\_{{02}}{\\mu}\_{{12}}^2+12{\\mu}\_{{20}}{\\mu}\_{{11}}^2{\\mu}\_{{21}}{\\mu}\_{{03}}+6{\\mu}\_{{20}}{\\mu}\_{{11}}{\\mu}\_{{02}}{\\mu}\_{{30}}{\\mu}\_{{03}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {-\\frac{18{\\mu}\_{{20}}{\\mu}\_{{11}}{\\mu}\_{{02}}{\\mu}\_{{21}}{\\mu}\_{{12}}+8{\\mu}\_{{11}}^3{\\mu}\_{{30}}{\\mu}\_{{03}}+6{\\mu}\_{{20}}{\\mu}\_{{02}}^2{\\mu}\_{{30}}{\\mu}\_{{12}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{9{\\mu}\_{{20}}{\\mu}\_{{02}}^2{\\mu}\_{{21}}^2+12{\\mu}\_{{11}}^2{\\mu}\_{{02}}{\\mu}\_{{30}}{\\mu}\_{{12}}-6{\\mu}\_{{11}}{\\mu}\_{{02}}^2{\\mu}\_{{30}}{\\mu}\_{{21}}}{{\\mu}\_{{00}}^{11}}} \\nonumber \\\\
    & & {+\\frac{{\\mu}\_{{02}}^3{\\mu}\_{{30}}^2}{{\\mu}\_{{00}}^{11}}}\\end{aligned}$$

Flusser's moments are invariant with respect to affine transformations.

Pixel based moments are similar to region-based moments, but they use
pixel values in addition. The brighter a pixel is the more weight it has
in the calculation of the moment.

If color images are used, they are converted to monochrome, before the
respective pixel-based moment is calculated. You can use the properties
i1, i2, i3, and i4 to read the respective usser moments.

Object Filtering
================

Filtering is the process of removing objects that satisfy a certain
condition. Examples of filtering are removal of small objects caused by
noise, or removal of objects touching the image border, because these
objects would be partial anyway and their features would be wrong.

Here is an example that removes objects with an area smaller than 5
pixels:

    rl = rl.filter([] (region & r) -> bool { return r.get_area() < 5; });

Here is another example that removes objects which are touching the left
border:

    rl = rl.filter([] (region & r) -> bool { return r.get_left() <= 0; });

By specifying a lambda, you can easily create various filter criteria.

Calculation Hierarchy
=====================

The order in which the features were presented to you so far may have
looked arbitrary to you. However, the order was guided by the hierarchy
used to calculate the features.

Feature calculation starts with the region, and many features can be
easily calculated using the region (e.g. area, left, etc.). However,
calculation of other features is more complicated, and they are
calculated indirectly. First the region is converted into a different
representation, and then this other representation is used to calculate
the feature. For example, the convex hull of the region is needed before
the convex area, convex perimeter and minimum and maximum feret
diameters can be calculated. Sometimes, this needs more than one step,
such as with the moments (see the chapter about moments, to see how they
are related to each other), which are needed to calculate the center of
gravity or the equivalent ellipse parameters, such as orientation or
eccentricity.

![](images\blob_calculation_hierarchy.png)

Convenience Classes
===================

The `blob_features` and `blob_features_list` classes are convenience
classes that allow you to calculate features without having to know
exactly how they are calculated internally. The `blob_features_list`
class provides parallelization on the blob level, i.e. features for
multiple blobs can be calculated in parallel. The `blob_features` class
also caches features that have been already calculated, in order to
avoid duplicate calculation.

However, it still helps to know what exactly is calculated and how, in
order to understand the algorithms runtime behavior.

Benchmarks
==========

We have provided a sample program called time\_blob\_analysis which can
be used to time the calculation of the various features. The times are
calculated for each feature in isolation, i.e. they do not reflect the
time savings you would gain, if some feature is already partly
calculated (i.e. if you calculate the convex perimeter, and have
previously calculated the convex hull, this will save considerable time,
since the shared portions of the calculation are not done twice).

This is the picture used in the benchmark program:

![](images\grains.png)

Here are the results of the time\_blob\_analysis program from my machine
(Lenovo Laptop with Intel Core i7-2820QM CPU running at 2.3 GHz):

**Blob Analysis Benchmark**

Image: grains.tif  
Size: 640 x 480  
Number of blobs: 93  
Mean blob area: 786.849

<table>
<colgroup>
<col width="44%" />
<col width="13%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left">Operation</td>
<td align="left">msec</td>
</tr>
<tr class="even">
<td align="left">Segmentation</td>
<td align="left">0.92367</td>
</tr>
<tr class="odd">
<td align="left">Connected components analysis</td>
<td align="left">8.54171</td>
</tr>
</tbody>
</table>

**Region-based blob features:**

<table>
<colgroup>
<col width="54%" />
<col width="19%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left">Operation</td>
<td align="left">msec</td>
</tr>
<tr class="even">
<td align="left">Area</td>
<td align="left">0.000192107</td>
</tr>
<tr class="odd">
<td align="left">Left</td>
<td align="left">0.000307371</td>
</tr>
<tr class="even">
<td align="left">Right</td>
<td align="left">0.000201712</td>
</tr>
<tr class="odd">
<td align="left">Top</td>
<td align="left">0.000172896</td>
</tr>
<tr class="even">
<td align="left">Bottom</td>
<td align="left">0.000302569</td>
</tr>
<tr class="odd">
<td align="left">Width</td>
<td align="left">0.000518689</td>
</tr>
<tr class="even">
<td align="left">Height</td>
<td align="left">0.00028816</td>
</tr>
<tr class="odd">
<td align="left">Aspect Ratio</td>
<td align="left">0.000662769</td>
</tr>
<tr class="even">
<td align="left">Bounds</td>
<td align="left">0.000739612</td>
</tr>
<tr class="odd">
<td align="left">Perimeter</td>
<td align="left">0.0963032</td>
</tr>
<tr class="even">
<td align="left">Compactness</td>
<td align="left">0.0724195</td>
</tr>
<tr class="odd">
<td align="left">Convex hull</td>
<td align="left">0.197909</td>
</tr>
<tr class="even">
<td align="left">Convex area</td>
<td align="left">0.171965</td>
</tr>
<tr class="odd">
<td align="left">Perforation</td>
<td align="left">0.0116224</td>
</tr>
<tr class="even">
<td align="left">Convexity</td>
<td align="left">0.216019</td>
</tr>
<tr class="odd">
<td align="left">Convex perimeter</td>
<td align="left">0.223185</td>
</tr>
<tr class="even">
<td align="left">Minimum feret diameter</td>
<td align="left">0.181483</td>
</tr>
<tr class="odd">
<td align="left">Maximum feret diameter</td>
<td align="left">0.154262</td>
</tr>
<tr class="even">
<td align="left">Minimum bounding circle</td>
<td align="left">0.228378</td>
</tr>
<tr class="odd">
<td align="left">Inner contour</td>
<td align="left">0.00650762</td>
</tr>
<tr class="even">
<td align="left">Inner contour perimeter</td>
<td align="left">0.00738651</td>
</tr>
<tr class="odd">
<td align="left">Outer contour</td>
<td align="left">0.00396221</td>
</tr>
<tr class="even">
<td align="left">Outer contour perimeter</td>
<td align="left">0.003943</td>
</tr>
<tr class="odd">
<td align="left">Region-based raw moments</td>
<td align="left">0.000763625</td>
</tr>
<tr class="even">
<td align="left">Region-based normalized moments</td>
<td align="left">0.000619545</td>
</tr>
<tr class="odd">
<td align="left">Region-based centroid</td>
<td align="left">0.00068198</td>
</tr>
<tr class="even">
<td align="left">Region-based central moments</td>
<td align="left">0.000653164</td>
</tr>
<tr class="odd">
<td align="left">Region-based equivalent ellipse</td>
<td align="left">0.000869284</td>
</tr>
<tr class="even">
<td align="left">Region-based scale-invariant moments</td>
<td align="left">0.000893297</td>
</tr>
<tr class="odd">
<td align="left">Region-based hu moments</td>
<td align="left">0.000802047</td>
</tr>
<tr class="even">
<td align="left">Region-based flusser moments</td>
<td align="left">0.000662769</td>
</tr>
<tr class="odd">
<td align="left">Holes</td>
<td align="left">0.0103689</td>
</tr>
<tr class="even">
<td align="left">Number of Holes</td>
<td align="left">0.00924991</td>
</tr>
<tr class="odd">
<td align="left">Area of Holes</td>
<td align="left">0.00936517</td>
</tr>
<tr class="even">
<td align="left">Fiber Length</td>
<td align="left">0.0801851</td>
</tr>
<tr class="odd">
<td align="left">Fiber Width</td>
<td align="left">0.00629148</td>
</tr>
<tr class="even">
<td align="left">Bounding rectangle</td>
<td align="left">0.162821</td>
</tr>
<tr class="odd">
<td align="left">Minimum area bounding rectangle</td>
<td align="left">0.178055</td>
</tr>
<tr class="even">
<td align="left">Minimum perimeter bounding rectangle</td>
<td align="left">0.169674</td>
</tr>
</tbody>
</table>

(Times are in msec per blob on average.)

**Pixel-based blob features:**

<table>
<colgroup>
<col width="54%" />
<col width="19%" />
</colgroup>
<tbody>
<tr class="odd">
<td align="left">Operation</td>
<td align="left">msec</td>
</tr>
<tr class="even">
<td align="left">Minimum position</td>
<td align="left">0.00191627</td>
</tr>
<tr class="odd">
<td align="left">Minimum</td>
<td align="left">0.0016137</td>
</tr>
<tr class="even">
<td align="left">Maximum position</td>
<td align="left">0.00167613</td>
</tr>
<tr class="odd">
<td align="left">Maximum</td>
<td align="left">0.00163771</td>
</tr>
<tr class="even">
<td align="left">Total</td>
<td align="left">0.00242055</td>
</tr>
<tr class="odd">
<td align="left">Mean</td>
<td align="left">0.00160409</td>
</tr>
<tr class="even">
<td align="left">Standard deviation</td>
<td align="left">0.00217561</td>
</tr>
<tr class="odd">
<td align="left">Skewness</td>
<td align="left">0.00225726</td>
</tr>
<tr class="even">
<td align="left">Channel minimum</td>
<td align="left">0.00174817</td>
</tr>
<tr class="odd">
<td align="left">Channel maximum</td>
<td align="left">0.00164251</td>
</tr>
<tr class="even">
<td align="left">Horizontal profile</td>
<td align="left">0.00903383</td>
</tr>
<tr class="odd">
<td align="left">Vertical profile</td>
<td align="left">0.00735289</td>
</tr>
<tr class="even">
<td align="left">Histogram</td>
<td align="left">0.00690625</td>
</tr>
<tr class="odd">
<td align="left">Pixel-based raw moments</td>
<td align="left">0.0784997</td>
</tr>
<tr class="even">
<td align="left">Pixel-based normalized moments</td>
<td align="left">0.0706569</td>
</tr>
<tr class="odd">
<td align="left">Pixel-based centroid</td>
<td align="left">0.0821401</td>
</tr>
<tr class="even">
<td align="left">Pixel-based central moments</td>
<td align="left">0.0826492</td>
</tr>
<tr class="odd">
<td align="left">Pixel-based equivalent ellipse</td>
<td align="left">0.0829854</td>
</tr>
<tr class="even">
<td align="left">Pixel-based scale-invariant moments</td>
<td align="left">0.0812612</td>
</tr>
<tr class="odd">
<td align="left">Pixel-based hu moments</td>
<td align="left">0.0729622</td>
</tr>
<tr class="even">
<td align="left">Pixel-based flusser moments</td>
<td align="left">0.0816263</td>
</tr>
</tbody>
</table>
