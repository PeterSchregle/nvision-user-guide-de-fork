**Nodes** sind die Bausteine in einer **Pipeline**.

Ein Node kann **Input Pins** und/oder **Output Pins** besitzen. Die Pins werden verwendet un die **Nodes** mit **Connections** zu verbinden.

![](./images/node.png)

Dies ist ein **Node** mit einem **Input Pin** mit dem Namen *In* und einem **Output Pin** mit dem Namen *Out*. Er übernimmt den Text von seinem Eingang, wandelt jedes Zeichen in den entsprechenden Großbuchstaben und stellt das Ergebnis an seinem Ausgang bereit.

![](./images/node1.png)

