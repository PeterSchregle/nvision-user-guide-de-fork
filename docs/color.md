Der Begriff Farbraum beschreibt die Repräsentation von Farben mittels
Zahlen. Es gibt eine unbegrenzte Anzahl von Farbräumen, und eine große
Zahl davon ist in ständiger Verwendung.

Physikalisch gesehen ist Farbe eine Erscheinung die mit einem Spektrum
beschrieben werden kann, einer Mischung von Lichtwellen
unterschiedlicher Wellenlängen zwischen 380 nm und 700 nm. Dieses
Spektrum stellt eine physikalische Größe dar, welche durch geeignete
Mittel gemessen werden kann.

![Das sichtbar Spektrum](images/The_Visible_Spectrum.png)

Auf der anderen Seite ist Farbe aber auch eine Empfindung, die im
menschlichen Gehirn entsteht, mittels Stimulation durch das Auge. Aus
diesem Grund ist es einigermaßen schwierig über Farbe zu sprechen, weil
manche Menschen Farben unterschiedlich empfinden. Es gibt Meschen mit
bestimmten Formen von Farbblindheit, deren Farbempfinden deutlich von
den Menschen mit normalem Farbsehen abweicht.

Da es leicht unterschiedliches Farbempfinden auch unter den normal
farb-sehenden Menschen gibt, hat die CIE einen Normalbeobachter mit
bestimmtem Farbempfinden definiert.

![Die Farb-Matching Funktionen des CIE
Normalbeobachters](images/The_Standard_Observer.png)

Der Normalbeobachter wurde 1931 dadurch definiert, dass ca. 200
normalsichtige Personen getestet wurden. Diese Personen wurden gebeten,
eine Farbe mit drei farbigen Lichtquellen (rot, grün und blau) zu
mischen, die Sie in einem 2° Ausschnitt in die Mitte ihrer Retina
projiziert bekamen. Die Zahlenverhältnisse der Mischungen wurden
aufgezeichnet und dazu verwendet, das Normal-Spektrum zu errechnen. 1964
wurde ein weiteres, ähnliches Experiment durchgeführt, um das 10°
Normal-Spektrum zu ermitteln.

Primnärfarben (rot, grün und blau) werden in den meisten Fernsehgeräten
und Computer-Monitoren gemischt, um das Spektrum einer gewünschten Farbe
anzunähern.

![The Farb Spektrum](images/The_Color_Spectrum.png)

Verwendung von Farbräumen im Druck und Fernsehen
================================================

Die Reproduktion von Bildern im Druck oder im Fernsehen ist äußerst
schwierig, da die Bilder of in dramatisch unterschiedlichen Umgebungen
betrachtet werden, als in denen sie aufgenommen wurden. Ein Beispiel
dafür ist eine Szene, die an einem hellen sonnigen Tag gefilmt wurde und
durch die Zuseher in einem dunklen Kino bei totaler Dunkelheit
betrachtet wird. Dennoch erwarten die Zuseher einen natürlichen
Farbeindruck. Ähnliche Probleme existieren beim Druck, wo Papier, Tinte
und Prozessunterschide es sehr schwierig machen, zu befriedigenden
Ergebnissen zu kommen.

Die Farbraumtransformationen in nVision können zwar auch für diese
Anwendungen benutzt werden, aber nVision wurde nicht als System zum
Farbmanagement konzipiert.

Farbräume in der Bildverarbeitung
=================================

Bei der Farbbildverarbeitung werden Farbräume of als ein Mittel benutzt,
um Farben qualitativ zu separieren. Je nach den spezifischen Anwendungen
gibt es Farbräume die besser dafür geeignet sind, weswegen der Bedarf
nach Farbraumtransformation entsteht.

Die in nVision verfügbaren Farbraumtransformationen wurden für die
Anforderungen der Bildverarbeitung in diesem Sinne entwickelt.

Die CIE Color Farbräume
=======================

Mittels der Farb-Matching Funktionen des Normalbeobachters hat die CIE
den XYZ Farbraum definiert. Die Tri-Stimulus Werte einer Farbe mit einer
spektralen Leistungsverteilung I(λ) werden so angegeben:

$$X=\\int I(\\lambda )\\bar{x}(\\lambda )d\\lambda$$

$$Y=\\int I(\\lambda )\\bar{y}(\\lambda )d\\lambda$$

$$Z=\\int I(\\lambda )\\bar{z}(\\lambda )d\\lambda$$

Da das menschliche Auge drei Typen von Farb-Sensoren hat, ist eine volle
Darstellung nur dreidimensional möglich. Wenn Helligkeit entfernt wird
bleibt nur Chromatizität übrig. Beispielsweise ist braun eine dunklere
Version von gelb; beide haben dieselbe Chromatizität, aber
unterschiedlichen Helligkeit.

Der XYZ Farbraum wurde so angelegt, dass der Y Parameter ein Maß für die
Helligkeit einer Farbe ist. Die Chromatizität einer Farbe kann dann mit
den Parametern x und y angegeben werden:

$$x=\\frac{X}{X+Y+Z}$$

$$y=\\frac{Y}{X+Y+Z}$$

Normalisierung erlabut es, ohne den dritten Parameter z auszukommen:

$$z=\\frac{Z}{X+Y+Z}=1-x-y$$

Der durch x, y, und Y definierte Farbraum ist bekannt als CIE xyY und
ist in breiter Verwendung um Farben in der Praxis zu spezifizieren.

![Die CIE Farbtafel](images/The_Chromaticity_Diagram.png)

Der RGB Farbraum
================

Es gibt nicht nur einen RGB Farbraum. Stattdessen sind weitere
Informationen nötig, um die RGB Variante präzise zu beschreiben.
Namentlich werden die XYZ Koordinaten der Primärfarben rot, grün und
blau benötigt, als auch die des Weißpunkts.

Eine sehr gute Informationsquelle ist die Webseite von Bruce Lindbloom,
insbesonde die Seite "RGB Working Space Information”.

Die meisten Bilder benutzen heutzutage den sRGB Farbraum, der auch von
nVision verwendet wird.

Die HLS und HSI Farbräume
=========================

Diese Farbmodele basieren auf den Konzepten Färbung, Tönung und
Schattierung eines Künstlers. Das Koordinatensystem ist zylindrisch und
die Modelle dienen der intuitiven Farb-Spezifikation.

Das HLS (hue - Farbton, lightness - Helligkeit, saturation - Sättigung)
Farbmodell stellt einen Doppelkegel dar. Der Farbton ist der Winkel um
die vertikale Achse, wobei rot bei 0°, grün bei 120° und blau bei 240°
liegt. Die Farben sind in gleicher Ordnung um den Umfang herum angelegt
wie in der CIE Farbtafel. Die Helligkeit ist entlang der vertikalen
Achse aufgetragen und variiert von schwarz am unteren Ende über die
verschiedenen Grautöne bis zu weiß an der Spitze. Sättigung ist der
radiale Abstand zur zentralen Achse und gibt an, in welchem Maß Farbe
und Grauton gemischt sind.

Das HSI (hue - Farbton, saturation - Sättigung, intensity - Intensität)
Farbmodell stellt einen Kegel dar, wobei die Kegelgrundfläche oben in
der Mitte weiß ist und entlang des Umfangs die voll gesättigten Farben
besitzt. Wie auch beim HLS Modell, liegt rot bei 0°, grün bei 120° und
blau bei 240°.

Die zugrunde liegende Mathematik beider Modelle ist in "Foley, van Dam:
Computer Graphics: Principles and Practice" beschrieben.

Beide Farbmodelle machen es recht einfach, Bilder anhand von Farben zu
segmentieren.

Der L\*a\*b\* Farbraum
======================

Der L\*a\*b\* Farbraum (manchmal auch CIELAB genannt) stellt
Komplementärfarben auf den Achsen a und b gegenüber. Er ist physikalisch
nichtlinear, um gleichmäßige Wahrnehmung zu ermöglichen. Die Differenz
zwischen zwei Farbtönen kann einfach durch die eukldische Distanz
ermittelt werden. Während HLS/HSI Intuitivität bieten, wird durch
L\*a\*b\* zusätzlich Gleichförmigkeit bereitgestellt.

Die zugrunde liegende Mathematik des L\*a\*b\* Farbraumns ist in
"Reinhard: Color Imaging" beschrieben.

Farbraum Umwandlung
===================

nVision has Funktionen um Bilder in verschiedenen Farbräumen
umzuwandeln.
