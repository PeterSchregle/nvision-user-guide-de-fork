**Nodes** können **Input Pins** und/oder **Output Pins** besitzen. **Pins** sind typisiert.

![](./images/input_pin_type.png) 
![](./images/output_pin_type.png)

**Connections** können nur zwischen **Pins** mit kompatiblen **Typen** gezogen werden.

![](./images/connection_ok.png)

**Pins** sind kompatibel, wenn ihre **Typen** gleich sind, oder wenn der auf der **Connection** fließende **Typ** automatisch konvertiert werden kann.

**Pins** mit inkompatiblen **Typen** können nicht verbunden werden.

![](./images/connection_nok.png)