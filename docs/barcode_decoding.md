**nVision** kann Barcodes dekodieren.

Verschiedene ein-dimensionale Symbologien werden unterstützt. Hier ist
eine alphabetische Liste der unterstützten Symbologien:

<table>
<colgroup>
<col width="8%" />
<col width="10%" />
<col width="81%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Symbologie</th>
<th align="left">Dimensionalität</th>
<th align="left">link to description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Code39</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Code_39"><a href="http://en.wikipedia.org/wiki/Code_39" class="uri">http://en.wikipedia.org/wiki/Code_39</a></a></td>
</tr>
<tr class="even">
<td align="left">Code93</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Code_93"><a href="http://en.wikipedia.org/wiki/Cod3_93" class="uri">http://en.wikipedia.org/wiki/Cod3_93</a></a></td>
</tr>
<tr class="odd">
<td align="left">Code128</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Code_128"><a href="http://en.wikipedia.org/wiki/Code_128" class="uri">http://en.wikipedia.org/wiki/Code_128</a></a></td>
</tr>
<tr class="even">
<td align="left">EAN</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/International_Article_Number_(EAN)"><a href="http://en.wikipedia.org/wiki/International_Article_Number_(EAN)" class="uri">http://en.wikipedia.org/wiki/International_Article_Number_(EAN)</a></a></td>
</tr>
<tr class="odd">
<td align="left">ITF</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/ITF-14"><a href="http://en.wikipedia.org/wiki/ITF-14" class="uri">http://en.wikipedia.org/wiki/ITF-14</a></a></td>
</tr>
<tr class="even">
<td align="left">UPC</td>
<td align="left">1D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Universal_Product_Code"><a href="http://en.wikipedia.org/wiki/Universal_Product_Code" class="uri">http://en.wikipedia.org/wiki/Universal_Product_Code</a></a></td>
</tr>
</tbody>
</table>

Der Dekoder Knoten hat eine Selektionsliste, mit der die gewünschten
Symbologien für die Dekodierung ausgewählt werden können.

![Die Selektiionsliste der Symbologien für den Barcode
Dekoder.](images/symbology_selection.png)

Der Dekoder erwartet einen Code in dem gegebenen Bild. Er erwartet den
Code in etwa in der Mitte, und mit horizontaler Abtastrichtung. Das
heißt, dass die Balken vertikal sein sollten. Eine virtuelle horizontale
Abtastlinie in der Mitte sollte den ganzen Code durchschneiden.

Hier ist ein Beispiel für einen perfekt positionierten Code:

![Perfekt positionierter Code für den Dekoder.](images/Code39h.png)

Der Code im folgenden Bild kann nicht dekodiert werden:

![Der Dekoder kann dieses Bild nicht dekodieren, weil die Balken
horizontal sind.](images/Code39v.png)

Der Code im folgenden Bild kann nicht dekodiert werden, weil die
virtuelle Abtastzeile nicht den ganzen Code durchschneidet:

![Der Dekoder kann dieses Bild nicht dekodieren, weil die virtuelle
Abtastzeile nicht den ganzen Code schneidet.](images/Code3930.png)

Der Barcode Dekoder unternimmt keine weiteren Schritte, um einen Code zu
lokalisieren. Wenn Ihre Umstände anders sind, können Sie andere
Werkzeuge zur Vorverarbeitung verwenden, um den Barcode in eine günstige
Position für den Dekoder zu bringen. Falls der Barocde ungünstig
verdreht ist, können Sie die Werkzeuge für die Rotation verwenden. Falls
der Barcode ungünstig positioniert ist, können Sie mit dem Crop Werkzeug
den für den Dekoder relevanten Bildteil ausschneiden. Falls Kontrast
oder Helligkeit nicht ausreichen, können Sie Bildvorverarbeitung
vornehmen.

Der Dekoder Knoten kann in eine lineare Pipeline oder in eine
Sub-Pipeline eingefügt werden.

![In einer linearen Pipeline fließt das Bild von oben in den Dekoder und
die Symbologien können ausgewählt
werden.](images/decoder_linear_pipeline.png)

In einer Sub-Pipeline gibt es zusätzliche Eingänge: Region für eine ROI,
bzw. TryHarder für aufwändigere Dekodierung, die aber auch länger dauern
kann.

![In einer Sub-Pipeline können mehrere Eingänge bessere Kontrolle
ausüben.](images/decoder_sub_pipeline.png)

Zusätzlich hat man auch am Ausgang des Dekoders mehrere Möglichkeiten.
Der Dekoder liefert eine Liste von Resultaten. Jedes Resultat enthält
die dekodierte Zeichenkeitte, den Namen und den Bezeichner der erkannten
Symbologie, sowie die Position, an der der Code gefunden wurde.
