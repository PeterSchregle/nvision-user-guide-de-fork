**nVision** kann die Position von Kanten in Bildern messen. Es verwendet
Projektionen entlang von Linien, innerhalb von Rechtecken oder entlang
von anderen Kurven oder innerhalb anderer Formen. Die Projektionen
werden analysiert und die Kanten mit Subpixel-Genauigkeit gemessen. Die
Präzision die Sie erreichen können hängt von der Charakteristik Ihres
Systems ab (Rauschen image Bild, Schärfe der Kanten, usw.), aber im
Allgemeinen können Sie eine Präzision von 1/20stel eines Pixels mit
einem optimalen System erwarten.

Der erste Schritt ist die Wahl einer Mess-Region. Dies kann ein
Liniensegment, ein beliebig orientiertes Rechteck, oder sogar ein
Kreisring sein. Ausgehend von dieser Region wird ein Intensitätsprofil
berechnet. Normalerweise würden Sie vermutlich die realen Intensitäten
eines Bilds verwenden, aber Sie können auch in einem anderen Farbraum
messen, wenn Sie an der Position eines Farbwechsels interessiert sind.

Das Intensitätsprofil wird geglättet um Rauschen zu unterdrücken und es
wird der Gradient berechnet. Anschließend werden die Extrema im
Gradienten gesucht und deren Stärke wird mit einer Schwelle verglichen.
Falls die Extrema unter die Schwelle fallen, werden sie verworfen;
anderenfalls repräsentieren die Extrema wirkliche Kanten. Die Positionen
dieser Kanten werden dann mit Sub-Pixel Präzision berechnet, als auch
die interpolierten Werte von Grauwert und Gradient an dieser Position.

Hier sehen Sie ein Werkzeug, welches die Informationen visualisiert.

![Dieses Werkzeug liefert Ihnen die Informationen um die richtigen
Parameter für die Kantenmessung zu finden, falls die Standard-Werte für
Ihr spezielles Bild nicht passen sollten. Oben am Werkzeug sehen Sie ein
Rechteck mit einem gepunkteten Rand, das Sie im Bild herumziehen können.
In diesem Rechteck sehen Sie kleine gelbe Liniensegmente an den Stellen
an denen Kanten detektiert wurden. Darunter ist ein Bereich in dem
sowohl das Grauwertprofile (ausgefüllt) als auch das Gradientenprofil
(nur Linie) angezeigt werden, zusammen mit zwei horizontalen Linien,
welche die Schwelle repräsentieren. Die vertikale Skalierung für das
Grauwertprofil ist zwischen 0 (schwarz) und 255 (weiß). Die vertikale
Skalierung des Gradientprofils ist zwischen -50 und +50. Kanten von
dunkel nach hell haben positive Gradienten, Kanten von hell nach dunkel
haben negative Gradienten. Dieses Werkzeug dient der Erforschung der
Kantenmessung in Ihrem speziellen Bild, und es zeigt die Auswirkungen
der Parameter zur Kantendetektion (wie z.B. Sigma für die Stärke der
Glättung und Threshold für die Selektion gültiger
Kanten).](images/gauging.png)
