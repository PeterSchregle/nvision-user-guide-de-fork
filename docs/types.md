Die in Pipelines fließenden Daten sind typisiert.

**Primitive Typen** sind:

| Typ                              | Typ-Name         | Beschreibung |
| -------------------------------- | ---------------- | ------------ |
| boolean                          | `System.Boolean` |
| integer (32 bit, mit Vorzeichen) | `System.Int32`   |      
| text                             | `System.String`  | Textdaten, z.B. *Text*. |

**Input Pins** und **Output Pins** sind typisiert. Bei den meisten Pins sind die Typen fix. Bei manchen Pins können sich die Typen ändern, während die Pipeline editiert wird.

Sich ändernde Pins sind die Input und Output Pins von Subpipelines, und ihre Listen-verarbeitenden Verwandten. Diese Pins übernehmen einen spezifischen Typ von der ersten Verbindung, die zu ihnen gezogen wird, entweder von außen oder von innen.
