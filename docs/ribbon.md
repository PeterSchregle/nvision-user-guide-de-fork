Das Menüband enthält Befehle, mit denen **nVision** bedient werden kann.
Die Befehle sind in Seiten und weiter in Gruppen eingeteilt.

![Das Menüband.](images/ribbon.png)

Oben am Menüband finden sich die Kommandos für Schnellzugriff.

<table>
<colgroup>
<col width="17%" />
<col width="17%" />
<col width="64%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/file_open.png" /></td>
<td align="left">CTRL+O</td>
<td align="left">Eine Datei öffnen</td>
</tr>
<tr>
<td align="left"><img src="../images/save.png" /></td>
<td align="left"></td>
<td align="left">Eine Datei speichern.</td>
</tr>
<tr>
<td align="left"><img src="../images/save_as.png" /></td>
<td align="left"></td>
<td align="left">Eine Datei unter neuem Namen speichern.</td>
</tr>
<tr>
<td align="left"><img src="../images/save_all.png" /></td>
<td align="left"></td>
<td align="left">Alle Dateien speichern.</td>
</tr>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Rechts im Menüband, neben dem **nVision** Logo ist der Hilfe Befehl.
Wenn Sie den Befehl wählen, wird das Hilfe-Fenster angezeigt. Das Hilfe-Fenster
liefert kontextspezifische Hilfe sowie Internet Links zu Schulungs-Videos und
zu diesem Handbuch.

<table>
<colgroup>
<col width="17%" />
<col width="17%" />
<col width="65%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/help.png" /></td>
<td align="left">F1</td>
<td align="left">Dokumentaton in einem Browser Fenster anzeigen.</td>
</tr>
</tbody>
</table>

Daneben, immer noch zur Rechten, ist eine Taste mit einem kleinen
aufwärts zeigenden Pfeil. Klicken Sie auf die Taste, um das Menüband zu
verkleinern.

![Das Menüband im verkleinerten Zustand.](images/ribbon_minimized.png)

Wenn das Menüband im verkleinerten Modus ist, können Sie die Befehle
dennoch erreichen, indem Sie zunächst auf die entsprechende Seite
klicken.

File
====

Die Seite ganz links zeigt die Kommandos zum Laden und Speichern von
Dateien, sowie zum Importieren und Exportieren von Pipelines.

![Das File Menü mit der Liste der zuletzt geöffneten
Dateien.](images/file_menu.png)

Außerdem wird eine Liste der kürzlich geladenen Dokumente angezeigt.
Dokumente auf der Liste können festgesteckt werden, so dass sie oben auf
der Liste angezeigt werden.

Unten gibt es eine Tast um **nVision** zu beenden.

Home
====

Die **Home** Seite zeigt die gebräuchlichsten Befehle.

![Die Home Seite.](images/home.png)

Links - in der Acquire Gruppe - gibt es Auswahl-Listen für die Kameras
und für die Farbmodi der Kamera (die Listen sind mit passenden Einträgen
gefüllt, sofern die Kameras richtig installiert sind) und zwei Tasten,
mit denen eine Schnappschuss (Start Snap) von einer Kamera gemacht
werden kann, oder die Kamera in den Live-Modus (Start Live/Stop Live)
geschaltet werden kann.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/snap.png" /></td>
<td align="left"></td>
<td align="left">Start/Stop Snap</td>
<td align="left">Macht einen Schnappschuss.</td>
</tr>
<tr>
<td align="left"><img src="../images/live.png" /></td>
<td align="left"></td>
<td align="left">Start/Stop Live</td>
<td align="left">Startet oder stoppt die Live Aufnahme.</td>
</tr>
<tr>
<td align="left"><img src="../images/exposure_control.png" /></td>
<td align="left"></td>
<td align="left">Exposure</td>
<td align="left">Fügt das Werkzeug zur Belichtungskontrolle ein.</td>
</tr>
</tbody>
</table>

In der **Zoom** Gruppe gibt es Befehle um den Zoom Faktor zu setzen. Sie
können hineinzoomen (**Zoom In**), herauszoomen (**Zoom Out**), oder den Zoom
zurücksetzen (**Reset Zoom**). Die **Reset Zoom** Taste hat ein Ausklapp-Menü
mit zusätzlichen Zoom Befehlen: **Zoom to Fit Width**, **Zoom to Fit Height**
und **Zoom to Fit**. Diese Befehler zoomen ein Bild dergestalt, dass es
genau in die Fensterbreite oder Fensterhöhe passt.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/zoom_in.png" /></td>
<td align="left">CTRL+ +</td>
<td align="left">Zoom In</td>
<td align="left">Einzoomen, um mehr Details zu zeigen.</td>
</tr>
<tr>
<td align="left"><img src="../images/zoom_out.png" /></td>
<td align="left">CTRL+ -</td>
<td align="left">Zoom Out</td>
<td align="left">Auszoomen, um mehr Umgebung zu zeigen.</td>
</tr>
<tr>
<td align="left"><img src="../images/zoom_reset.png" /></td>
<td align="left">CTRL+ALT+0</td>
<td align="left">Reset Zoom</td>
<td align="left">Zoom Faktor zurücksetzen.</td>
</tr>
</tbody>
</table>

Der **Reset Zoom** Befehl hat ein Untermenü mit zusätzlichen Befehlen:

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/zoom_fit.png" /></td>
<td align="left"></td>
<td align="left">Zoom Fit</td>
<td align="left">Zoom so wählen, dass das Bild in maximaler Grösse ins Fenster passt.</td>
</tr>
<tr>
<td align="left"><img src="../images/zoom_fit_width.png" /></td>
<td align="left"></td>
<td align="left">Zoom to Fit Width</td>
<td align="left">Zoom so wählen, dass das Bild die Fensterhöhe ausnutzt.</td>
</tr>
<tr>
<td align="left"><img src="../images/zoom_fit_height.png" /></td>
<td align="left"></td>
<td align="left">Zoom to Fit Height</td>
<td align="left">Zoom so wählen, dass das Bild die Fensterbreite ausnutzt.</td>
</tr>
</tbody>
</table>

In der **Tool Windows** Gruppe gibt es Befehle, mit denen Sie zusätzliche Fenster
anzeigen oder verbergen können.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/show_workspace.png" /></td>
<td align="left">CTRL+ +</td>
<td align="left">Show Browser</td>
<td align="left">Browser Fenster anzeigen oder verbergen.</td>
</tr>
<tr>
<td align="left"><img src="../images/help.png" /></td>
<td align="left">F1</td>
<td align="left">Show Help</td>
<td align="left">Hilfe Fenster anzeigen oder verbergen.</td>
</tr>
<tr>
<td align="left"><img src="../images/show_camera_parameters.png" /></td>
<td align="left"></td>
<td align="left">Show Camera Parameters</td>
<td align="left">Kamera Parameter Fenster anzeigen oder verbergen.</td>
</tr>
<tr>
<td align="left"><img src="../images/show_system_globals.png" /></td>
<td align="left"></td>
<td align="left">Show System Globals</td>
<td align="left">System Globals Fenster anzeigen oder verbergen.</td>
</tr>
</tbody>
</table>

Locate
======

Die Locate Seite zeigt Werkzeuge für die Lokalisierung von Teilen. Die 
Lokalisierung von Teilen ist oft der erste Schritt in einer Inspektions-
Aufgabe.

![The locate tab.](images/locate_tab.png)

Die **Edit** Gruppe enthält die Befehle zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Locate** Gruppe enthält die Befehle zur Lokalisierung. 

<table>
<colgroup>
<col width="15%" />
<col width="13%" />
<col width="70%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/locate_shape.png" /></td>
<td align="left"></td>
<td align="left">Lokalisiert ein Teil anhand seiner Form (Geometrisches Form-Matching).</td>
</tr>
<tr>
<td align="left"><img src="../images/locate_pattern.png" /></td>
<td align="left"></td>
<td align="left">Lokalisiert ein Teil anhand eines Mustervergleichs (Normalisierte Korrelation).</td>
</tr>
<tr>
<td align="left"><img src="../images/teach.png" /></td>
<td align="left"></td>
<td align="left">Einlernen des Teils für nachfolgende Lokalisierung.</td>
</tr>
</tbody>
</table>

Measure
=======

Die Measure Seite zeigt Werkzuege für die Vermessung von Teilen. Das Messen
von Intensitäten und Farben, von geometrischen Dimensionen, und auch das 
Zählen von Eigenschaften sind oft Schritte in einer Inspektionsaufgabe.

![The measure tab.](images/measure_tab.png)

Die **Edit** Gruppe enthält die Befehle zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Intensity** Gruppe enthält die Befehle für Intensitäts-basierte Messungen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/features_brightness.png" /></td>
<td align="left"></td>
<td align="left">Brightness</td>
<td align="left">Inspiziert die Helligkeit in einer Region.</td>
</tr>
<tr>
<td align="left"><img src="../images/features_contrast.png" /></td>
<td align="left"></td>
<td align="left">Contrast</td>
<td align="left">Inspiziert den Kontrast in einer Region.</td>
</tr>
</tbody>
</table>

Die **Geometry** Gruppe enthält die Befehle für Geometrie-basierte Messungen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/geometry_distance.png" /></td>
<td align="left"></td>
<td align="left">Distance</td>
<td align="left">Misst eine Länge.</td>
</tr>
<tr>
<td align="left"><img src="../images/geometry_circle.png" /></td>
<td align="left"></td>
<td align="left">Circle</td>
<td align="left">Misst einen Kreis.</td>
</tr>
<tr>
<td align="left"><img src="../images/geometry_angle.png" /></td>
<td align="left"></td>
<td align="left">Angle</td>
<td align="left">Misst einen Winkel.</td>
</tr>
<tr>
<td align="left"><img src="../images/features_area_size.png" /></td>
<td align="left"></td>
<td align="left">Area Size</td>
<td align="left">Misst eine Fläche.</td>
</tr>
</tbody>
</table>

Die **Count** Gruppe enthält die Befehle zum Zählen von Features.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/geometry_count_edges.png" /></td>
<td align="left"></td>
<td align="left">Count Edges</td>
<td align="left">Zählt die Anzahl von Kanten entlang einer Linie.</td>
</tr>
<tr>
<td align="left"><img src="../images/features_count_contour_points.png" /></td>
<td align="left"></td>
<td align="left">Count Contour Points</td>
<td align="left">Zählt die Anzahl von Kontur-Punkten.</td>
</tr>
<tr>
<td align="left"><img src="../images/features_count_areas.png" /></td>
<td align="left"></td>
<td align="left">Count Areas</td>
<td align="left">Zählt die Anzahl von unzusammenhängenden Blobs.</td>
</tr>
</tbody>
</table>

Verify
======

Die **Verify** Seite hat die Kommandos zur Verifizierung von Teilen.

![The verify tab.](images/verify_tab.png)

Die **Edit** Gruppe enthält die Befehle zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Features** Gruppe enthält die Befehle zum Mustervergleich.
 
<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/features_match_contour.png" /></td>
<td align="left"></td>
<td align="left">Match Contour</td>
<td align="left">Vergleicht eine Region eines Teils mit geometrischem Mustervergleich.</td>
</tr>
<tr>
<td align="left"><img src="../images/features_match_pattern.png" /></td>
<td align="left"></td>
<td align="left">Match Pattern</td>
<td align="left">Vergleicht eine Region eines Teils mit normalisierter Korrelation.</td>
</tr>
</tbody>
</table>

Identify
========

Die **Identify** Seite zeigt die Kommdos für die Identifikation von Teilen,
wie Dekodierung von Barcodes und Matrix Codes, als auch das Lesen von Text.

![The identify tab.](images/identify_tab.png)

Die **Edit** Gruppe enthält die Befehle zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Codes** Gruppe enthält die Kommandos zur Dekodierung von Barcodes und 
Matrix Codes.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/identify_barcode.png" /></td>
<td align="left"></td>
<td align="left">Barcode</td>
<td align="left">Dekodiert einen Barcode in einer Region.</td>
</tr>
<tr>
<td align="left"><img src="../images/identify_matrixcode.png" /></td>
<td align="left"></td>
<td align="left">Matrixcode</td>
<td align="left">Dekodiert einen Matrix Code in einer Region.</td>
</tr>
</tbody>
</table>

Die **Text** Gruppe enthält die Befehle für die optische Zeichenerkennung.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/ocr.png" /></td>
<td align="left"></td>
<td align="left">OCR</td>
<td align="left">Liest Text in einer Region mit optischer Zeichenerkennung (OCR).</td>
</tr>
</tbody>
</table>

Process
=======

Die **Process** Seite hat grundlegende Bildverarbeitungsbefehle. Die Befehle sind
eingeteilt in statistische Operationen, Punkt-Operationen (Pixel für Pixel), 
Farbraumtransformationen, Filter, Morphologische Operationen und geometrische
Transformationen.

![The process tab.](images/process_tab.png)

Die **Edit** Gruppe enthält die Befehle zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Statistics** Gruppe enthält Befehle für statistische Verarbeitung.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/histogram.png" /></td>
<td align="left"></td>
<td align="left">Histogram</td>
<td align="left">Berechnet ein Histogram.</td>
</tr>
<tr>
<td align="left"><img src="../images/horizontal_profile.png" /></td>
<td align="left"></td>
<td align="left">Horizontal Profile</td>
<td align="left">Berechnet ein horizontales Profil.</td>
</tr>
<tr>
<td align="left"><img src="../images/horizontal_profile.png" /></td>
<td align="left"></td>
<td align="left">Horizontal Profile Overlay</td>
<td align="left">Berechnet ein horizontales Profil und zeigt es als Overlay über dem Bild.</td>
</tr>
</tbody>
</table>

Die **Crop** Gruppe enthält den **Crop Box** Befehl.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/crop.png" /></td>
<td align="left"></td>
<td align="left">Crop Box</td>
<td align="left">Schneidet einen rechteckigen achsenparallelen Ausschnitt aus dem Bild aus.</td>
</tr>
</tbody>
</table>

Die **Bin** Gruppe enthält das **Bin** Kommando.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/bin.png" /></td>
<td align="left"></td>
<td align="left">Bin</td>
<td align="left">Verkleinert ein Bild mittels Binning.</td>
</tr>
</tbody>
</table>

Die **Point** Gruppe enthält Punktoperationen zwischen einem Bild und einem
konstanten Wert. Diese Operationen betrachten Pixel ohne ihre Nachbarschaft
zu berücksichtigen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/not.png" /></td>
<td align="left"></td>
<td align="left">Not</td>
<td align="left">Invertiert jedes Pixel eines Bilds (1er Komplement).</td>
</tr>
<tr>
<td align="left"><img src="../images/negate.png" /></td>
<td align="left"></td>
<td align="left">Negate</td>
<td align="left">Invertiert jedes Pixel eines Bilds (2er Komplement).</td>
</tr>
<tr>
<td align="left"><img src="../images/increment.png" /></td>
<td align="left"></td>
<td align="left">Increment</td>
<td align="left">Inkrementiert jedes Pixel eines Bilds um 1.</td>
</tr>
<tr>
<td align="left"><img src="../images/decrement.png" /></td>
<td align="left"></td>
<td align="left">Decrement</td>
<td align="left">Dekrementiert jedes Pixel eines Bilds um 1.</td>
</tr>
<tr>
<td align="left"><img src="../images/abs.png" /></td>
<td align="left"></td>
<td align="left">Absolute</td>
<td align="left">Berechnet den Absolutwert jedes Pixels eines Bilds.</td>
</tr>
<tr>
<td align="left"><img src="../images/add.png" /></td>
<td align="left"></td>
<td align="left">Add</td>
<td align="left">Addiert einen konstanten Wert zu jedem Pixel eines Bilds.</td>
</tr>
<tr>
<td align="left"><img src="../images/subtract.png" /></td>
<td align="left"></td>
<td align="left">Subtract</td>
<td align="left">Subtrahiert einen konstanten Wert von jedem Pixel eines Bilds.</td>
</tr>
<tr>
<td align="left"><img src="../images/difference.png" /></td>
<td align="left"></td>
<td align="left">Difference</td>
<td align="left">Berechnet die absolute Differenz zwischen jedem Pixel eines Bilds und einer Konstante.</td>
</tr>
<tr>
<td align="left"><img src="../images/multiply.png" /></td>
<td align="left"></td>
<td align="left">Multiply</td>
<td align="left">Addiert jedes Pixel eines Bilds mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/divide.png" /></td>
<td align="left"></td>
<td align="left">Divide</td>
<td align="left">Dividiert jedes Pixel eines Bilds durch einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/and.png" /></td>
<td align="left"></td>
<td align="left">Logical And</td>
<td align="left">Logische Und-Verknüpfung zwischen jedem Bild eines Pixels mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/or.png" /></td>
<td align="left"></td>
<td align="left">Logical Or</td>
<td align="left">Logische Oder-Verknüpfung zwischen jedem Bild eines Pixels mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/xor.png" /></td>
<td align="left"></td>
<td align="left">Logical Xor</td>
<td align="left">Logische Xor-Verknüpfung zwischen jedem Bild eines Pixels mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/equal.png" /></td>
<td align="left"></td>
<td align="left">Equal</td>
<td align="left">Vergleicht jedes Pixel eines Bilds mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/bigger.png" /></td>
<td align="left"></td>
<td align="left">Bigger</td>
<td align="left">Vergleicht jedes Pixel eines Bilds mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/bigger_or_equal.png" /></td>
<td align="left"></td>
<td align="left">Bigger or Equal</td>
<td align="left">Vergleicht jedes Pixel eines Bilds mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/smaller.png" /></td>
<td align="left"></td>
<td align="left">Smaller</td>
<td align="left">Vergleicht jedes Pixel eines Bilds mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/smaller_or_equal.png" /></td>
<td align="left"></td>
<td align="left">Smaller or Equal</td>
<td align="left">Vergleicht jedes Pixel eines Bilds mit einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/min.png" /></td>
<td align="left"></td>
<td align="left">Minimum</td>
<td align="left">Berechnet das Minimum zwischen jedem Pixel eines Bilds und einem konstanten Wert.</td>
</tr>
<tr>
<td align="left"><img src="../images/max.png" /></td>
<td align="left"></td>
<td align="left">Maximum</td>
<td align="left">Berechnet das Maximum zwischen jedem Pixel eines Bilds und einem konstanten Wert.</td>
</tr>
</tbody>
</table>

Die **Color** Gruppe enthält die Befehle zur Farbraum-Konvertierung.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/convert_monochrome.png" /></td>
<td align="left"></td>
<td align="left">Convert to Monochrome</td>
<td align="left">Konvertiert jedes Pixel eines Farbbilds nach monochrom.</td>
</tr>
<tr>
<td align="left"><img src="../images/rgb.png" /></td>
<td align="left"></td>
<td align="left">Convert to Rgb</td>
<td align="left">Konvertiert jedes Pixel eines Farbbilds in den RGB (red, green, blue) Farbraum.</td>
</tr>
<tr>
<td align="left"><img src="../images/hls.png" /></td>
<td align="left"></td>
<td align="left">Convert to Hls</td>
<td align="left">Konvertiert jedes Pixel eines Farbbilds in den HLS (hue, luminance, saturation) Farbraum.</td>
</tr>
<tr>
<td align="left"><img src="../images/hsi.png" /></td>
<td align="left"></td>
<td align="left">Convert to Hsi</td>
<td align="left">Konvertiert jedes Pixel eines Farbbilds in den HSI (hue, saturation, intensity) Farbraum.</td>
</tr>
<tr>
<td align="left"><img src="../images/lab.png" /></td>
<td align="left"></td>
<td align="left">Convert to Lab</td>
<td align="left">Konvertiert jedes Pixel eines Farbbilds in den L*A*B* Farbraum.</td>
</tr>
</tbody>
</table>

Die **Frame** Gruppe enthält den **Frame** Befehl.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/frame_filter.png" /></td>
<td align="left"></td>
<td align="left">Frame</td>
<td align="left">Berechnet einen Rahmen um ein Bild herum.</td>
</tr>
</tbody>
</table>

Die **Filter** Gruppe enthält lineare Filter. Lineare Filter berechnen ihre
Ergebnisse unter Berücksichtigung ihrer Nachbarpixel. Verschiedenene Filter
verwenden veschiedenene Filterkenre um die unterschiedlichen Charakteristiken
der Filter zu erzeugen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/median_filter.png" /></td>
<td align="left"></td>
<td align="left">Median</td>
<td align="left">Berechnet einen Median Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/median_filter.png" /></td>
<td align="left"></td>
<td align="left">Hybrid median</td>
<td align="left">Berechnet einen hybriden Median Filter, der Kanten besser erhält.</td>
</tr>
<tr>
<td align="left"><img src="../images/blur.png" /></td>
<td align="left"></td>
<td align="left">Gaussian</td>
<td align="left">Glättet ein Bild mit einem Gauss-Mittelwertfilter mit variabler Größe.</td>
</tr>
<tr>
<td align="left"><img src="../images/blur.png" /></td>
<td align="left"></td>
<td align="left">Gauss</td>
<td align="left">Glättet ein Bild mit einem Gauss-Mittelwertfilter mit fixer Größe.</td>
</tr>
<tr>
<td align="left"><img src="../images/blur.png" /></td>
<td align="left"></td>
<td align="left">Lowpass</td>
<td align="left">Glättet ein Bild mit einem Tiefpass.</td>
</tr>
<tr>
<td align="left"><img src="../images/sharpen.png" /></td>
<td align="left"></td>
<td align="left">Hipass</td>
<td align="left">Schärft ein Bild mit einem Hochpass.</td>
</tr>
<tr>
<td align="left"><img src="../images/sharpen.png" /></td>
<td align="left"></td>
<td align="left">Laplace</td>
<td align="left">Berechnet einen Laplace Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/horizontal_edge.png" /></td>
<td align="left"></td>
<td align="left">Horizontal Sobel</td>
<td align="left">Verstärkt Kanten mit einem horizontalen Sobel Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/horizontal_edge.png" /></td>
<td align="left"></td>
<td align="left">Horizontal Prewitt</td>
<td align="left">Verstärkt Kanten mit einem horizontalen Prewitt Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/horizontal_edge.png" /></td>
<td align="left"></td>
<td align="left">Horizontal Scharr</td>
<td align="left">Verstärkt Kanten mit einem horizontalen Scharr Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/vertical_edge.png" /></td>
<td align="left"></td>
<td align="left">Vertical Sobel</td>
<td align="left">Verstärkt Kanten mit einem vertikalen Sobel Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/vertical_edge.png" /></td>
<td align="left"></td>
<td align="left">Vertical Prewitt</td>
<td align="left">Verstärkt Kanten mit einem vertikalen Prewitt Filter.</td>
</tr>
<tr>
<td align="left"><img src="../images/vertical_edge.png" /></td>
<td align="left"></td>
<td align="left">Vertical Scharr</td>
<td align="left">Verstärkt Kanten mit einem vertikalen Scharr Filter.</td>
</tr>
</tbody>
</table>

Die **Morphology** Gruppe enthält morphologische, d.h. Form-verändernde
Operationen. Einige dieser Operationen sind auf Bilder oder auf Regionen
anwendbar. Die Versionen für Regionen sind binärer Natur und durch ihre 
zusätzliche lauflängen-kodierte Implementierung extrem schnell.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/dilation.png" /></td>
<td align="left"></td>
<td align="left">Dilate</td>
<td align="left">Führt eine Dilatation der Bildpixel durch. Eine Dilatation macht helle Bereiche größer und dunkle Bereiche kleiner.</td>
</tr>
<tr>
<td align="left"><img src="../images/dilation_regions.png" /></td>
<td align="left"></td>
<td align="left">Dilate (Regions)</td>
<td align="left">Führt eine Dilatation der Region durch. Eine Dilatation macht die Region größer.</td>
</tr>
<tr>
<td align="left"><img src="../images/erosion.png" /></td>
<td align="left"></td>
<td align="left">Erode</td>
<td align="left">Führt eine Erosion der Bildpixel durch. Eine Erosion macht helle Bereiche kleiner und dunkle Bereiche größer.</td>
</tr>
<tr>
<td align="left"><img src="../images/erosion_regions.png" /></td>
<td align="left"></td>
<td align="left">Erode (Regions)</td>
<td align="left">Führt eine Erosion der Region durch. Eine Erosion macht die Region kleiner.</td>
</tr>
<tr>
<td align="left"><img src="../images/opening.png" /></td>
<td align="left"></td>
<td align="left">Open</td>
<td align="left">Führt eine Öffnung der Bildpixel durch. Eine Öffnung ist eine Erosion, gefolgt von einer Dilatation.</td>
</tr>
<tr>
<td align="left"><img src="../images/opening_regions.png" /></td>
<td align="left"></td>
<td align="left">Open (Regions)</td>
<td align="left">Führt eine Öffnung der Region durch. Eine Öffnung ist eine Erosion, gefolgt von einer Dilatation.</td>
</tr>
<tr>
<td align="left"><img src="../images/closing.png" /></td>
<td align="left"></td>
<td align="left">Close</td>
<td align="left">Führt eine Schließung der Bildpixel durch. Eine Schließung ist eine Dilatation, gefolgt von einer Erosion.</td>
</tr>
<tr>
<td align="left"><img src="../images/closing_regions.png" /></td>
<td align="left"></td>
<td align="left">Close (Regions)</td>
<td align="left">Führt eine Schließung der Region durch. Eine Schließung ist eine Dilatation, gefolgt von einer Erosion.</td>
</tr>
<tr>
<td align="left"><img src="../images/morphological_gradient.png" /></td>
<td align="left"></td>
<td align="left">Gradient</td>
<td align="left">Berechnet den morphologischen Gradienten eines Bilds. Ein morphologischer Gradient ist eine Dilatation minus eine Erosion.</td>
</tr>
<tr>
<td align="left"><img src="../images/morphological_gradient_regions.png" /></td>
<td align="left"></td>
<td align="left">Gradient (Regions)</td>
<td align="left">Berechnet den morphologischen Gradienten einer Region. Ein morphologischer Gradient ist eine Dilatation minus eine Erosion.</td>
</tr>
<tr>
<td align="left"><img src="../images/boundary.png" /></td>
<td align="left"></td>
<td align="left">Inner Boundary</td>
<td align="left">Berechnet die innere Grenze einer Region. Die innere Grenze besteht aus den Regionen-Pixeln, die den Hintergrund berühren.</td>
</tr>
<tr>
<td align="left"><img src="../images/boundary.png" /></td>
<td align="left"></td>
<td align="left">Outer Boundary</td>
<td align="left">Berechnet die äußere Grenze einer Region. Die äußere Grenze besteht aus den Hintergrund-Pixeln, die die Region berühren.</td>
</tr>
<tr>
<td align="left"><img src="../images/fill_holes.png" /></td>
<td align="left"></td>
<td align="left">Fill Holes</td>
<td align="left">Füllt die Löcher in einer Region.</td>
</tr>
</tbody>
</table>

Die **Geometry** Gruppe enthält die Befehle für geometrische Transformationen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/mirror.png" /></td>
<td align="left"></td>
<td align="left">Mirror</td>
<td align="left">Vertauscht rechts mit links bei einem Bild.</td>
</tr>
<tr>
<td align="left"><img src="../images/flip.png" /></td>
<td align="left"></td>
<td align="left">Flip</td>
<td align="left">Vertauscht oben und unten bei einem Bild.</td>
</tr>
<tr>
<td align="left"><img src="../images/rotate90ccw.png" /></td>
<td align="left"></td>
<td align="left">Rotate Clockwise</td>
<td align="left">Dreht ein Bild und 90 Grad gegen den Uhrzeigersinn.</td>
</tr>
<tr>
<td align="left"><img src="../images/rotate180.png" /></td>
<td align="left"></td>
<td align="left">Rotate 180 Degrees</td>
<td align="left">Dreht ein Bild um 180 Grad.</td>
</tr>
<tr>
<td align="left"><img src="../images/rotate90cw.png" /></td>
<td align="left"></td>
<td align="left">Rotate Counter Clockwise</td>
<td align="left">Dreht ein Bild und 90 Grad im Uhrzeigersinn.</td>
</tr>
</tbody>
</table>

Segmentation
============

Auf dieser Seite sind die Befehle zur Segmentierung und Blob Analyse.

![The segmentation tab.](images/segmentation_tab.png)

Die **Edit** Gruppe enthält die Befehle zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Segmentation** Gruppe enthält die Befehle für den Schwellwertvergleich,
die Zusammenhangsanalyse und die Filterung der Regionen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/thresholding.png" /></td>
<td align="left"></td>
<td align="left">Threshold</td>
<td align="left">Die Schwellwert-Verarbeitung eines Bildes liefert eine Region.</td>
</tr>
<tr>
<td align="left"><img src="../images/connected_components.png" /></td>
<td align="left"></td>
<td align="left">Connected Components</td>
<td align="left">Teilt eine Region in ihre zusammenhängenden Komponenten, d.h. eine Liste von zusammenhängenden Regionen.</td>
</tr>
<tr>
<td align="left"><img src="../images/filter.png" /></td>
<td align="left"></td>
<td align="left">Filter Regions</td>
<td align="left">Filtern der Regionenliste anhand eines Kriteriums.</td>
</tr>
</tbody>
</table>

Die **Blob Analysis** Gruppe enthält die Befehler für Blob Analyse und graphische
Darstellung.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/blob_analysis.png" /></td>
<td align="left"></td>
<td align="left">Blob Analysis</td>
<td align="left">Blob Analyse einer Liste von Regionen.</td>
</tr>
<tr>
<td align="left"><img src="../images/Chart-Line.png" /></td>
<td align="left"></td>
<td align="left">Plot</td>
<td align="left">Plotten der Ergebnisse der Blob Analyse.</td>
</tr>
</tbody>
</table>

Pipeline
========

Auf dieser Seite finden sich Befehle mit Bezug zur Pipeline.

![The pipeline tab.](images/pipeline_tab.png)

Die **Edit** Gruppe enthält die Befehler zum Editieren der Pipeline. Diese
Befehle sind auf den meisten Seiten dupliziert, um eine gute Erreichbarkeit
zu bieten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/delete.png" /></td>
<td align="left"></td>
<td align="left">Delete Node</td>
<td align="left">Löscht den ausgewählten Knoten.</td>
</tr>
<tr>
<td align="left"><img src="../images/commit_document.png" /></td>
<td align="left"></td>
<td align="left">Commit</td>
<td align="left">Übergibt das Resultat des ausgewählten Knotens als Dokument.</td>
</tr>
</tbody>
</table>

Die **Pipeline** Gruppe enthält die Befehle zur Erzeugung von Sub-Pipelines
sowie dem Import und Export von Pipelines.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/sub_pipeline.png" /></td>
<td align="left"></td>
<td align="left">Sub-Pipeline</td>
<td align="left">Erzeugt eine Sub-pipeline.</td>
</tr>
<tr>
<td align="left"><img src="../images/pipeline_import.png" /></td>
<td align="left"></td>
<td align="left">Import Pipeline</td>
<td align="left">Importiert eine Pipeline von einer Datei.</td>
</tr>
<tr>
<td align="left"><img src="../images/pipeline_export.png" /></td>
<td align="left"></td>
<td align="left">Export Pipeline</td>
<td align="left">Exportiert eine Pipeline in eine Datei.</td>
</tr>
</tbody>
</table>


Die **Control** Gruppe enthält die Befehler für die Stapelverarbeitung von
Bildern aus Verzeichnissen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/mm-First.png" /></td>
<td align="left"></td>
<td align="left">First Image</td>
<td align="left">Springt zum ersten Bild in einem Verzeichnis.</td>
</tr>
<tr>
<td align="left"><img src="../images/mm-Previous.png" /></td>
<td align="left"></td>
<td align="left">Previous Image</td>
<td align="left">Springt zum vorherigen Bild in einem Verzeichnis.</td>
</tr>
<tr>
<td align="left"><img src="../images/mm-Play.png" /></td>
<td align="left"></td>
<td align="left">Next Image</td>
<td align="left">Springt zum nächsten Bild in einem Verzeichnis.</td>
</tr>
<tr>
<td align="left"><img src="../images/Play.png" /></td>
<td align="left"></td>
<td align="left">Process Images</td>
<td align="left">Verarbeitet alle Bilder in einem Verzeichnis.</td>
</tr>
<tr>
<td align="left"><img src="../images/mm-Stop.png" /></td>
<td align="left"></td>
<td align="left">Stop</td>
<td align="left">Stoppt die Stapel-Verarbeitung.</td>
</tr>
<tr>
<td align="left"><img src="../images/Save-Tick.png" /></td>
<td align="left"></td>
<td align="left">Export On/Off</td>
<td align="left">Schaltet Export-Knoten an oder aus.</td>
</tr>
<tr>
<td align="left"><img src="../images/Save-And-New.png" /></td>
<td align="left"></td>
<td align="left">Export Once</td>
<td align="left">Alle Export Knoten exportieren einmalig.</td>
</tr>
</tbody>
</table>

Die **Timer** Gruppe erlaut es, das Abfrage-Intervall für die pollenden Knoten
zu setzen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/timer.png" /></td>
<td align="left"></td>
<td align="left">Update On/Off</td>
<td align="left">Polling an oder aus-schalten.</td>
</tr>
</tbody>
</table>

Die **Execution Info** Gruppe zeigt Informationen zur Programm-Ausführung. Es
zeigt die letztmalige Ausführungszeit und den Ausführungs-Zustand. 

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/timer.png" /></td>
<td align="left"></td>
<td align="left">Show Node Timing</td>
<td align="left">Zeigt die Ausführungszeit jedes Knotens in einer Sub-pipeline.</td>
</tr>
</tbody>
</table>

Die **Execution Statistics** Gruppe zeigt statistische Informationen über die
Programmausführung. Sie zeigt die mittlere Ausführungszeit sowie die erreichbare
Bildrate. 

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/execute.png" /></td>
<td align="left"></td>
<td align="left">Statistics Start/Stop</td>
<td align="left">Statistik-Berechnung ein und aus-schalten.</td>
</tr>
</tbody>
</table>

Die **Result** Gruppe enthält das **Result** Werkzeug, das eine einfache
Kommunikation mit einer Maschine realisiert.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/tool_result.png" /></td>
<td align="left"></td>
<td align="left">Result</td>
<td align="left">Dieses Werkzeug bietet eine einfache Kommunikation mit einer Maschine.</td>
</tr>
</tbody>
</table>

Interactive Measurement
=======================

Die **Measure** Seite zeigt Befehle im Zusammenhang mit interaktiven Messungen.

![The measure tab.](images/measure_tab.png)

Die **Calibrate** Gruppe enthält die Befehler für interaktive Kalibrierung.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/calibrate_origin.png" /></td>
<td align="left"></td>
<td align="left">Calibrate Origin</td>
<td align="left">Ursprungs-Kalibrationswerkzeug an/ausschalten.</td>
</tr>
<tr>
<td align="left"><img src="../images/calibrate_scale.png" /></td>
<td align="left"></td>
<td align="left">Calibrate Scale</td>
<td align="left">Skalierungs-Kalibrationswerkzeug an/ausschalten.</td>
</tr>
</tbody>
</table>

Die **Overlay** Gruppe enthält die Kommandos um grafische Overlays an und 
auszuschalten.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/rectangular_grid.png" /></td>
<td align="left"></td>
<td align="left">Rectangular Grid</td>
<td align="left">Rechteck-Gitter an/ausschalten.</td>
</tr>
<tr>
<td align="left"><img src="../images/circular_grid.png" /></td>
<td align="left"></td>
<td align="left">Circular Grid</td>
<td align="left">Kreis-Gitter an/ausschalten.</td>
</tr>
<tr>
<td align="left"><img src="../images/scale_bar.png" /></td>
<td align="left"></td>
<td align="left">Scale Bar</td>
<td align="left">Skala an/ausschalten.</td>
</tr>
</tbody>
</table>

Die **Picker** Gruppe enthält den interaktiven Pixelwert-Picker.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/picker.png" /></td>
<td align="left"></td>
<td align="left">Picker</td>
<td align="left">Interactively inspect pixel values.</td>
</tr>
</tbody>
</table>

Die **Measure** Grupper enhält die Kommandos für interaktives Messen.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/commit_data_row.png" /></td>
<td align="left"></td>
<td align="left">Commit Results</td>
<td align="left">Ergebnis des ausgewählten Werkzeugs in Tabelle übernehmen.</td>
</tr>
<tr>
<td align="left"><img src="../images/counting_tool.png" /></td>
<td align="left"></td>
<td align="left">Counting Tool</td>
<td align="left">Zähl-Werkzeug an/ausschalten.</td>
</tr>
<tr>
<td align="left"><img src="../images/reset_zero.png" /></td>
<td align="left"></td>
<td align="left">Reset Counting Tool</td>
<td align="left">Zähl-Werkzeug zurücksetzen.</td>
</tr>
<tr>
<td align="left"><img src="../images/ruler_scale.png" /></td>
<td align="left"></td>
<td align="left">Measure Length Tool</td>
<td align="left">Längen-Werkzeug an/ausschalten.</td>
</tr>
</tbody>
</table>

Die **Gauging** Gruppe enthält den Kanten-Inspektor.

<table>
<colgroup>
<col width="10%" />
<col width="10%" />
<col width="20%" />
<col width="60%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kürzel</th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><img src="../images/gauge_hpos.png" /></td>
<td align="left"></td>
<td align="left">Horizontal Edge Inspector</td>
<td align="left">Werkzeug zur Parametrierung der Kantensuche.</td>
</tr>
</tbody>
</table>
