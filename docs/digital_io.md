**nVision** unterstützt digitale Eingabe und Ausgabe über EA Module.
Dies ist hilfreich um Maschinen von Machine Vision Applikationen aus zu
steuern oder um auf Signale von der Maschine zu reagieren. Die Digital
Eingabe und Ausgabe kann über E/A Module oder eine SPS
(Speicherprogrammierbare Steuerung) erfolgen.

Advantech Adam-60xx Ethernet Module
===================================

**nVision** unterstützt verschiedene Digital-E/A Module von Advatech
([<http:www.advantech.com>](http://www.advantech.com)).

![](images/ADAM-6050_S.jpg)

Die Module werden über LAN oder drahtloses WLAN mit einem PC verbunden
und bieten eine Auswahl an Eingabe/Ausgabe Leitungen. Die Module haben
zusätzliche Merkmale wie Zähler, die aber nicht unterstützt werden.

<table>
<colgroup>
<col width="20%" />
<col width="12%" />
<col width="61%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Modul</th>
<th align="left">Bus</th>
<th align="left">Merkmale</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">ADAM-6050</td>
<td align="left">LAN</td>
<td align="left">12 digitale Eingänge, 6 digitale Ausgänge</td>
</tr>
<tr class="even">
<td align="left">ADAM-6050W</td>
<td align="left">WLAN</td>
<td align="left">12 digitale Eingänge, 6 digitale Ausgänge</td>
</tr>
<tr class="odd">
<td align="left">ADAM-6051</td>
<td align="left">LAN</td>
<td align="left">12 digitale Eingänge, 2 digitale Ausgänge</td>
</tr>
<tr class="even">
<td align="left">ADAM-6051W</td>
<td align="left">WLAN</td>
<td align="left">12 digitale Eingänge, 2 digitale Ausgänge</td>
</tr>
<tr class="odd">
<td align="left">ADAM-6052</td>
<td align="left">LAN</td>
<td align="left">8 digitale Eingänge, 8 digitale Ausgänge</td>
</tr>
<tr class="even">
<td align="left">ADAM-6060</td>
<td align="left">LAN</td>
<td align="left">6 digitale Eingänge, 6 digitale Ausgänge</td>
</tr>
<tr class="odd">
<td align="left">ADAM-6060W</td>
<td align="left">WLAN</td>
<td align="left">6 digitale Eingänge, 6 digitale Ausgänge</td>
</tr>
<tr class="even">
<td align="left">ADAM-6066</td>
<td align="left">LAN</td>
<td align="left">6 digitale Ausgänge</td>
</tr>
</tbody>
</table>

Folgende Knoten sind verfügbar:

<table>
<colgroup>
<col width="14%" />
<col width="85%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Knoten</th>
<th align="left">Zweck</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Adam6000 Device</td>
<td align="left">Ein Advantech Adam-6000 Modul, benötigt zur Initialisierung und zum Lesen von Informationen.</td>
</tr>
<tr class="even">
<td align="left">Read Inputs</td>
<td align="left">Wird benötigt um von digitalen Eingängen zu lesen.</td>
</tr>
<tr class="odd">
<td align="left">Write Outputs</td>
<td align="left">Wird benötigt um auf digitale Ausgänge zu schreiben.</td>
</tr>
</tbody>
</table>

Der **Adam6000 Module** Knoten stellt eine Verbindung zu einem Adam
Gerät her. Die IP Address und Port Eingänge werden benötigt um das Modul
zu identifizieren. Der Type Eingang wird verwendet um den Typ des Moduls
auszuwählen. Der Module Ausgang kann mit den **Read Inputs** und **Write
Outputs** Knoten verbunden werden.

Der **Read Inputs** Knoten liest die Werte von den elektrischen
Eingängen. Der DigIn Ausgang enthält den Zustand der elektrischen
Eingänge in der Form einer binären ganzen Zahl.

Der **Write Outputs** Knoten schreibt den Wert seine DigOut Eingangs auf
die elektrischen Ausgänge.

Die Sync Eingägne der **Read Inputs** und **Write Outputs** Knoten
können die Reihenfolge definieren.

Der **Read Inputs** Knoten kann in regulären Intervallen mit dem
**nVision** Timer abgefragt werden (Polling).

Data Translation OpenLayers
===========================

**nVision** unterstützt verschiedene Digital-E/A Module von Data
Translation
([<http:www.datatranslation.com>](http://www.datatranslation.com)).

![](images/dt9817.jpg)

Die Module werden über USB oder PCI bus mit einem PC verbunden und
bieten eine Auswahl an Eingabe/Ausgabe Leitungen. Die Module haben
zusätzliche Merkmale wie Zähler, die aber nicht unterstützt werden.

<table>
<colgroup>
<col width="18%" />
<col width="11%" />
<col width="59%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Modul</th>
<th align="left">Bus</th>
<th align="left">Merkmale</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">DT9817</td>
<td align="left">USB</td>
<td align="left">28 digitale Eingänge/Ausgänge</td>
</tr>
<tr class="even">
<td align="left">DT9817-H</td>
<td align="left">USB</td>
<td align="left">28 digitale Eingänge/Ausgänge</td>
</tr>
<tr class="odd">
<td align="left">DT9817-R</td>
<td align="left">USB</td>
<td align="left">8 digitale Eingänge, 8 digital Ausgänge</td>
</tr>
<tr class="even">
<td align="left">DT9835</td>
<td align="left">USB</td>
<td align="left">64 digitale Eingänge/Ausgänge</td>
</tr>
<tr class="odd">
<td align="left">DT335</td>
<td align="left">PCI</td>
<td align="left">32 digitale Eingänge/Ausgänge</td>
</tr>
<tr class="even">
<td align="left">DT351</td>
<td align="left">PCI</td>
<td align="left">8 digitale Eingänge, 8 digital Ausgänge</td>
</tr>
<tr class="odd">
<td align="left">DT340</td>
<td align="left">PCI</td>
<td align="left">32 digitale Eingänge, 8 digital Ausgänge</td>
</tr>
</tbody>
</table>

Sie müssen die Data Translation OpenLayers Software installieren, um die
Module in **nVision** nutzen zu können. Wir haben OpenLayers 7.5
benutzt, um die Unterstützung zu implementieren, d.h. OpenLayers 7.5
oder höher wird benötigt.

Folgende Knoten sind verfügbar:

<table>
<colgroup>
<col width="16%" />
<col width="83%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Knoten</th>
<th align="left">Zweck</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">OpenLayers System</td>
<td align="left">Lesen von Information über die installierte DT OpenLayers Software und die verbundenen Module.</td>
</tr>
<tr class="even">
<td align="left">OpenLayers Device</td>
<td align="left">Ein DT OpenLayers Module, benötigt zur Initialisierung und zum Lesen von Informationen.</td>
</tr>
<tr class="odd">
<td align="left">Read Inputs</td>
<td align="left">Wird benötigt um von digitalen Eingängen zu lesen.</td>
</tr>
<tr class="even">
<td align="left">Write Outputs</td>
<td align="left">Wird benötigt um auf digitale Ausgänge zu schreiben.</td>
</tr>
</tbody>
</table>

Der **OpenLayers System** Knoten zählt die am PC angeschlossenen
OpenLayers Geräte auf. Der Modules Ausgang liefert eine Liste der
angeschlossenen Geräte. Der Version Ausgang zeigt die Version der
installierten OpenLayers software.

Der **OpenLayers Device** Knoten stellt eine Verbindung zu einem
OpenLayers Gerät her. Der Name Eingang wird benötigt um das Modul zu
identifizieren. Sie können den Namen herausfinden, indem Sie den Ausgang
des **OpenLayers System** Knoten untersuchen. Der Module Ausgang kann
mit den **Read Inputs** und **Write Outputs** Knoten verbunden werden.

Der **Read Inputs** Knoten liest die Werte von den elektrischen
Eingängen. Der DigIn Ausgang enthält den Zustand der elektrischen
Eingänge in der Form einer binären ganzen Zahl.

Der **Write Outputs** Knoten schreibt den Wert seine DigOut Eingangs auf
die elektrischen Ausgänge.

Die Sync Eingägne der **Read Inputs** und **Write Outputs** Knoten
können die Reihenfolge definieren.

Der **Read Inputs** Knoten kann in regulären Intervallen mit dem
**nVision** Timer abgefragt werden (Polling).

Das Beispiel zeigt, wie herausgefunden werden kann, welche DT OpenLayers
Module verbunden sind.

![](images/dtol_enumeration.png)

Wenn der Name eines DT DT OpenLayers Modul bekannt ist, kann er
eingetippt werden, um das Modul zu initialisieren. Der Wert, der auf
einem digitalen Ausgang ausgegeben werden soll wird typsicherweise nicht
eingetippt, sondern woanders berechnet. Ein Wert, der über einen
digitalen Eingang eingelesen wird, wird in der Regel anderswo in der
Pipeline verwendet.

![](images/dtol_read_write.png)

Beckhoff TwinCAT3
=================

**nVision** untersützt die direkte Verbindung zu einer Beckhoff SPS.
([<http:www.beckhoff.de>](http://www.beckhoff.de)).

Sie müssen die Beckhoff TwinCAT3 Software installieren, um die Module
von **nVision** aus zu verwenden.

Folgende Knoten sind verfügbar:

<table>
<colgroup>
<col width="16%" />
<col width="83%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">node</th>
<th align="left">purpose</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Beckhoff PLC</td>
<td align="left">Stellt die Verbindung zur SPS her.</td>
</tr>
<tr class="even">
<td align="left">Read Symbol</td>
<td align="left">Liest den Wert eines Symbols in der SPS.</td>
</tr>
<tr class="odd">
<td align="left">Write Symbol</td>
<td align="left">Schreibt den Wert eines Symbols in der SPS.</td>
</tr>
</tbody>
</table>

Der **Beckhoff PLC** Knoten stellt eine Verbindung zur SPS über das ADS
Protokoll her. Er benötigt eine AMS NetId und einen Port als Eingang, da
dies für die Indentifizierung der ADS Geräte benötigt wird. Der Ausgang
des **Beckhoff PLC** Knoten repäsentiert die SPS, der mit den **Read
Symbol** und **Write Symbol** Knoten verbunden werden kann.

Der **Read Symbol** Knoten liest den Wert eines Symbols in der SPS. Das
Symbol kann aus einer Liste am Symbol Eingang ausgewählt werden. Die
Liste enthält alle verfügbaren Symbole in der verbundenen SPS. Der
**Read Symbol** Knoten hat zwei Ausgänge. Der Value Ausgang liefert den
Wert des gelesenen Symbols. Der Info Ausgang liefert Informationen über
das Symbol. Diese Informationen können mit dem **Get Property** Knoten
ermittelt werden.

Der **Write Symbol** Knoten schreibt einen Wert auf ein Symbol in der
SPS. Das Symbol kann aus einer Liste am Symbol Eingang ausgewählt
werden. Die Liste enthält alle verfügbaren Symbole in der verbundenen
SPS. Der neue Wert des Symbols kann am Eingang Value gesetzt werden.

Die Sync Eingägne der **Read Symbol** und **Write Symbol** Knoten können
die Reihenfolge definieren.

Der **Read Symbols** Knoten kann in regulären Intervallen mit dem
**nVision** Timer abgefragt werden (Polling).
