**nVision** ist ein Entwicklungs-System für Computer-Sehen. Die
Hauptanwendung ist es, automatisierte Machine Vision Anwendungen für die
Industrie zu entwickeln. Es verwendet eine grafische Darstellung zur
Programmierung, was zu viel kürzeren Entwicklungszeiten führt,
verglichen mit traditioneller Programmierung. Außerdem ist es leicht zu
lernen, womit Sie augenblicklich produktiv sind.

Da **nVision** wertvolle und leistungsfähige Werkzeuge für die
Bildinspektion besitzt, erweist es sich auch im wissenschaftlichen Labor
nützlich. Es ist das Werkzeug der Wahl, when die Arbeit mit Photoshop zu
arbeitsaufwändig wird und wenn Sie schnelle Resultate benötigen. Die
grafische Programmierung hilft auch im Labor um schnelle Ergebnisse in
einer automatisierten Weise zu erhalten.

**nVision** hilft Ihnen bei einer Vielzahl von Vision-basierten
Aufgaben, die in der Industrie benötigt werden:

-   Prüfen Sie die Position eines Teils, um ein Handling-System zu
    steuern, oder um ein Vision-Werkzeug in die richtige Position zu
    bringen.
-   Identifizieren Sie ein Teil basierend auf seiner Markierung, Form
    oder von anderen visuellen Aspekten.
-   Prüfen Sie, ob ein Teil korrekt gefertigt oder zusammengebaut wurde.
-   Vermessen Sie die Größe eines Teils.
-   Inspizieren Sie ein Teil auf Defekte.

![Das nVision Fenster.](images/nVision.png)

##Programm Ausgaben

**nVision** hat zwei Hauptkomponenten: der **nVision Designer** ist das
Entwicklungssystem und die **nVision Runtime** ist der
Ausführungsanteil. Der **nVision Designer** hat ein graphisches
Benutzerinterface und ein graphisches Programmiersystem. Die **nVision Runtime** stellt die Computer Vision Funktionalität auf eine modulare
Weise zur Verfügung und führt die graphischen Programme aus. Sie ist
auch in der Lage, ein MMI (Mensch-Maschine-Interface) anzuzeigen.

Mit dem **nVision Designer** erzeugen Sie Ihre Anwendungen. Wenn diese
Anwendungen an der Maschine oder in der Fabrikhalle installiert werden,
wird üblicherweise die kostengünstigere und modulare **nVision Runtime**
verwendet.

###nVision Designer

Der **nVision Designer** wird mit einem kompletten Feature-Satz für die
beabsichtige Zielgruppe geliefert.

<table>
<colgroup>
<col width="27%" />
<col width="72%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Produkt</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><strong>nVision Designer MV</strong></td>
<td align="left">Für Machine Vision Entwicklung.</td>
</tr>
<tr class="even">
<td align="left"><strong>nVision Designer L</strong></td>
<td align="left">Für die Anwendung im wissenschaftlichen Labor.</td>
</tr>
<tr class="odd">
<td align="left"><strong>nVision Designer U</strong></td>
<td align="left">Die ultimative Version enthält alle verfügbaren Funktionen.</td>
</tbody>
</table>

###nVision Runtime

Folgende Module sind verfügbar:

<table>
<colgroup>
<col width="21%" />
<col width="78%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Modul</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><strong>Image Processing</strong></td>
<td align="left">Enthält grundlegende Bildverarbeitungsfunktionalität, monochrom and farbig, inkl. Kamera-Anbindung.</td>
</tr>
<tr class="even">
<td align="left"><strong>Analysis, Measurement</strong></td>
<td align="left">Enthält Bildanalysefunktionalität, Blob-Analyse, Kamera-Kalibrierung, Messfunktionen.</td>
</tr>
<tr class="odd">
<td align="left"><strong>Template Matching</strong></td>
<td align="left">Enthält Funktionen zum Mustervergleich und zur geometrischen Mustersuche.</td>
</tr>
<tr class="even">
<td align="left"><strong>Barcode, Matrixcode</strong></td>
<td align="left">Enthält Funktionen zum Barcode Dekodieren, sowohl lineare als auch zweidimensionale Symbologien.</td>
</tr>
<tr class="odd">
<td align="left"><strong>OCR, OCV</strong></td>
<td align="left">Enthält Funktionen zum Lesen und Verifizieren von Beschriftungen.</td>
</tr>
</tbody>
</table>

###Modulmatrix

Die Tabelle zeigt, welche Module in welcher Designer Version enthalten
sind.

<table>
<colgroup>
<col width="38%" />
<col width="9%" />
<col width="9%" />
<col width="9%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Modul</th>
<th align="left">MV</th>
<th align="left">L</th>
<th align="left">U</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><strong>Image Processing</strong></td>
<td align="left">*</td>
<td align="left">*</td>
<td align="left">*</td>
</tr>
<tr class="even">
<td align="left"><strong>Image Analysis</strong></td>
<td align="left">*</td>
<td align="left">*</td>
<td align="left">*</td>
</tr>
<tr class="odd">
<td align="left"><strong>Template Matching</strong></td>
<td align="left">*</td>
<td align="left"></td>
<td align="left">*</td>
</tr>
<tr class="even">
<td align="left"><strong>Barcode, Matrixcode</strong></td>
<td align="left">*</td>
<td align="left"></td>
<td align="left">*</td>
</tr>
<tr class="odd">
<td align="left"><strong>OCR, OCV</strong></td>
<td align="left">*</td>
<td align="left"></td>
<td align="left">*</td>
</tr>
<tr class="even">
<td align="left"><strong>3D Rendering</strong></td>
<td align="left"></td>
<td align="left">*</td>
<td align="left">*</td>
</tr>
</tbody>
</table>

##Anforderungen

**nVision** (Designer und Runtime) laufen auf Windows 7 oder höher, 64
bit (empfohlen) oder 32 bit (mit systembedingten Einschränkungen bzgl.
des verfügbaren Speichers). Es werden viele Kameras verschiedener
Hersteller unterstützt, entweder über die GigE Vision, USB3 Vision und
Genicam Standards, oder über direkte Einbindung des Hersteller SDKs.

##Lizenzierung

**nVision** (Designer und Runtime) werden auf spezifische Hardware
lizenziert. Sie benötigen einen Lizenzschlüssel, um **nVision** laufen
zu lassen.

Wenn **nVision** das erste Mal ausgeführt wird, muss es aktiviert
werden. Die Aktivierung benötigt zusätzlich zum Lizenzschlüssel einen
Hardwareschlüssel, der automatisch bestimmt wird. Der
Aktivierungsprozess benutzt den Hardwareschlüssel zusammen mit dem
Lizenzschlüssel und erzeugt einen Aktivierungsschlüssel. Dieser
Aktivierungsschlüssel, in Kombination mit den beiden anderen Schlüsseln,
schaltet die **nVision** Software frei.

Die Aktivierung wird normalerweise über das Internet ausgeführt und ist
einfach und transparent. Wenn Sie Ihren Lizenzschlüssel in die
Zwischenablage kopieren, bevor Sie **nVision** zum ersten Mal starten,
werden Sie diesen Prozess kaum bemerken. Andernfalls wird ein Dialog
ähnlich dem folgenden angezeigt:

![Der **nVision** Lizenzdialog.](images/nVisionLicensing1.png)

Um fortzufahren, geben Sie bitte Ihren Lizenzschlüssel ein und drücken
Sie die Return Taste. **nVision** wird den Lizenzserver kontaktieren und
den zurückgelieferten Aktivierungsschlüssel zur Freischaltung verwenden.
Falls der Lizenzserver aus irgendeinem Grund nicht kontaktiert werden
kann, können Sie uns anrufen (+49 8245 7749600) oder ein Email an
<info@impuls-imaging.com> schreiben und uns sowohl Ihren Lizenzschlüssel
als auch den Hardwareschlüssel mitteilen. Im Gegenzug werden wir Ihnen
einen Aktivierungsschlüssel geben, den Sie manuell eingeben können.

Die Aktivierung für eine spezifische Hardware ist ewig gültig. Wenn sich
die Hardware ändert, wird sie als eine neue Hardware betrachtet, und Sie
müssen in dem Fall **nVision** neu aktivieren. **nVision** ist zwar nur
für eine Maschine lizenziert, aber ein Lizenzschlüssel erlaubt dennoch
die dreimalige Aktivierung, um dem Fall einer wegen Defekt
ausgetauschten Hardware abzumildern.

##Schnittstellen zu Kameras und Geräten

Die primäre Schnittstelle zu Kmaeras in **nVision** sind die **GigEVision** 
und die **USB3 Vision** Schnittstellen. Diese Schnittstellen sind standardisiert. 
Bilddaten werden über **GenTL** angesprochen und Kamera-Eigenschaften können über 
**GenICam** gelesen bzw. geschrieben werden. 

Bei Kameras, die über **GenTL** und **GenICam** angesprochen werden, ist der 
ganze Satz von Eigenschaft in **nVision** ansprechbar.

Die Eigenschaften der Kameras sind in der Form eines Property-Fensters 
ansprechbar.

![Das Property-Fenster.](images/camera_properties.png)

Zusätzlich können die Eigenschaften der Kamera durch eine grafische Pipeline
gelesen und geschrieben werden, d.h. die Eigenschaften können durch ein 
Programm geändert werden.

![Schreiben von Kamera Eigenschaften in einer Pipeline.](images/camera_properties_pipeline.png)

Sekundäre Schnittstellen zu Kmaeras und Geräten existieren aus historischen
Gründen über die direkten SDKs der Hersteller. USB2 Kameras werden auf diese
Art angesprochen, da sie schon vor der Standardisierung existierten, und auch
Schnittstellen zu Framegrabbern.

Die folgende Tabelle zählt die getesteten Geräte auf:

<table>
<colgroup>
<col width="10%" />
<col width="5%" />
<col width="10%" />
<col width="10%" />
<col width="10%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Hersteller</th>
<th align="left"></th>
<th align="left">URL</th>
<th align="left">Gerät</th>
<th align="left">Schnittstelle</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><strong>Allied Vision</strong></td>
<td align="left"><img src="../images/allied_vision_logo.png"></td>
<td align="left"><a href="http://www.alliedvision.com/">www.alliedvision.com/</a></td>
<td align="left">GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="even">
<td align="left"><strong>Basler</strong></td>
<td align="left"><img src="../images/basler_logo.png"></td>
<td align="left"><a href="http://www.baslerweb.com/">www.baslerweb.com/</a></td>
<td align="left">GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="odd">
<td align="left"><strong>Baumer</strong></td>
<td align="left"><img src="../images/baumer_logo.png"></td>
<td align="left"><a href="http://www.baumer.com/">www.baumer.com/</a></td>
<td align="left">GigE / USB3 Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="even">
<td align="left"><strong>Flir</strong></td>
<td align="left"><img src="../images/flir_logo.png"></td>
<td align="left"><a href="http://www.flir.com/">www.flir.com/</a></td>
<td align="left">GigE Kameras (A315)</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="odd">
<td align="left"><strong>IDS</strong></td>
<td align="left"><img src="../images/ids_logo.png"></td>
<td align="left"><a href="http://www.ids-imaging.com/">www.ids-imaging.com/</a></td>
<td align="left">USB Kameras</td>
<td align="left">Direkt (Hewrsteller SDK)</td>
</tr>
<tr class="even">
<td align="left"><strong>IDS</strong></td>
<td align="left"><img src="../images/ids_logo.png"></td>
<td align="left"><a href="http://www.ids-imaging.com/">www.ids-imaging.com/</a></td>
<td align="left">USB3 / GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="odd">
<td align="left"><strong>JAI</strong></td>
<td align="left"><img src="../images/jai_logo.png"></td>
<td align="left"><a href="http://www.jai.com/">www.jai.com/</a></td>
<td align="left">USB3 / GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="even">
<td align="left"><strong>Leutron</strong></td>
<td align="left"><img src="../images/leutron_logo.png"></td>
<td align="left"><a href="http://www.leutron.com/">www.leutron.com/</a></td>
<td align="left">USB Kameras</td>
<td align="left">Direct (Vendor SDK)</td>
</tr>
<tr class="odd">
<td align="left"><strong>Gardasoft</strong></td>
<td align="left"><img src="../images/gardasoft_logo.png"></td>
<td align="left"><a href="http://www.gardasoft.com/">www.gardasoft.com/</a></td>
<td align="left">GigE Flash Controller</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="even">
<td align="left"><strong>Matrix Vision</strong></td>
<td align="left"><img src="../images/mv_logo.png"></td>
<td align="left"><a href="http://www.matrix-vision.com/">www.matrix-vision.com/</a></td>
<td align="left">Framegrabber</td>
<td align="left">Direkt (Hewrsteller SDK)</td>
</tr>
<tr class="odd">
<td align="left"><strong>Matrix Vision</strong></td>
<td align="left"><img src="../images/mv_logo.png"></td>
<td align="left"><a href="http://www.matrix-vision.com/">www.matrix-vision.com/</a></td>
<td align="left">USB Kameras</td>
<td align="left">Direkt (Hewrsteller SDK)</td>
</tr>
<tr class="even">
<td align="left"><strong>Matrix Vision</strong></td>
<td align="left"><img src="../images/mv_logo.png"></td>
<td align="left"><a href="http://www.matrix-vision.com/">www.matrix-vision.com/</a></td>
<td align="left">USB3 / GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="odd">
<td align="left"><strong>Net GmbH</strong></td>
<td align="left"><img src="../images/net_logo.png"></td>
<td align="left"><a href="http://net-gmbh.com">net-gmbh.com</a></td>
<td align="left">USB Kameras</td>
<td align="left">Direkt (Hewrsteller SDK)</td>
</tr>
<tr class="even">
<td align="left"><strong>Point Grey</strong></td>
<td align="left"><img src="../images/point_grey_logo.png"></td>
<td align="left"><a href="http://ptgrey.com">ptgrey.com</a></td>
<td align="left">USB3 / GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="odd">
<td align="left"><strong>Sentech</strong></td>
<td align="left"></td>
<td align="left"><a href="http://sentecheurope.com">sentecheurope.com</a></td>
<td align="left">USB3 / GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="even">
<td align="left"><strong>Smartek</strong></td>
<td align="left"><img src="../images/smartek_logo.png"></td>
<td align="left"><a href="http://smartek.vision">smartek.vision</a></td>
<td align="left">GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="odd">
<td align="left"><strong>The Imaging Source</strong></td>
<td align="left"><img src="../images/the_imaging_source_logo.png"></td>
<td align="left"><a href="http://www.theimagingsource.com">www.theimagingsource.com</a></td>
<td align="left">GigE Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
<tr class="even">
<td align="left"><strong>Various</strong></td>
<td align="left"><img src="../images/windows_media_foundation_logo.png"></td>
<td align="left"></td>
<td align="left">Webcams</td>
<td align="left">Direkt (Windows Media Foundation)</td>
</tr>
<tr class="odd">
<td align="left"><strong>Ximea</strong></td>
<td align="left"><img src="../images/ximea_logo.png"></td>
<td align="left"><a href="http://www.ximea.com">www.ximea.com</a></td>
<td align="left">USB3 Kameras</td>
<td align="left">GenTL / GenICam</td>
</tr>
</tbody>
</table>

Alle anderen **GigE Vision** oder **USB3 Vision** Kameras sollten mit 
**nVision** einfach funktionieren, wurden aber bei uns im Haus nicht getestet.