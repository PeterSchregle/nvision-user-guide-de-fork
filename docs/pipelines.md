**Pipelines** sind die Programme, die von **nVision** ausgeführt werden. Daten fließen durch eine Pipeline.

**Nodes** sind die Bausteine einer Pipeline. Nodes haben **Pins**, die verwendet werden, um Nodes über  **Connections** zu verbinden.

Hier ist eine einfache Pipeline, die aus zwei Nodes besteht:

![](./images/HelloWorld.png)

Eine Pipeline stellt einen gerichteten Graph dar. Die Daten fließen in der Richtung, die durch die Pfeile der Verbindungen dargestellt wird. Zyklen im Graph sind nicht erlaubt.

Im oben gezeigten Beispiel hat der *Text* Node einen **Input Pin** *StringValue*, an dem der Text `Hello World!` eingetippt wurde. Der **Output Pin** dieses Node wurde mit dem **Input Pin** des *Uppercase* Node verbunden. Jeder Node zeigt den Wert seines (ersten) **Output Pin**.

Pipelines werden kontinuierlich ausgeführt. Jedesmal wenn irgendein Wert sich verändert, läuft die Pipeline von neuem und erzeugt neue Ausgaben. Die Pipeline-Ausführung ist bedarfsgesteuert und erfolgt nur wenn nötig. Nodes werden nur ausgeführt, wenn es nötig ist.
