**Connections** zwischen zwei **Pins** stellen die Mittel für den Datenfluss dar. Da die **Pins** den **Typ** der Daten definieren, können **Connections** nur gemacht werden, wenn die **Typen** an beiden Enden der **Connection** gleich sind oder wenn die fließenden Daten konvertiert werden können.

Erweiternde Konvertierungen werden automatisch gemacht, wenn sie verfügbar sind. Hinter den Kulissen wird automatisch ein Konvertierungs **Node** erzeugt, der aber nicht sichtbar ist. 

Die Konvertierung einer ganzen Zahl zu einer Fließkommazahl ist eine erweiternde Konvertierung (eine ganze Zahl kann verlustfrei auch als Fließkommazahl dargestellt werden) und erfolgt automatisch.

Die Konvertierung einer Fließkommazahl in eine ganze Zahl ist verengend (die Nachkommastellen der Fließkommazahl müssen möglicherweise für die Konvertierung entfernt werden) und erfolgt nicht automatisch.