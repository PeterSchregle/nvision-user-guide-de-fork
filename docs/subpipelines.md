**Subpipelines** können verwendet werden, um eine Menge von verbundenen **Nodes** in einen einzelnen **Node** zu gruppieren.

Wie jeder andere **Node** auch, hat eine **Subpipeline** optionale **Input Pins** und/oder **Output Pins**. 

Standardmäßig hat ein **Subpipeline** **Node** einen **Input Pin** und einen **Output Pin**.

![](./images/subpipeline.png)

Der Name der **Subpipeline** kann editiert werden, wenn er angeklickt wird:

![](./images/subpipeline_title_edit.png)

In der **Subpipeline** (Doppel-Klick auf den **Node** um hineinzugehen) sieht es so aus:

![](./images/subpipeline_inside.png)

Zusätzliche **Pins** eines **Subpipeline** **Node** können mit den *Add Input Pin* und *Add Output Pin* Befehlen im Kontextmenü definiert werden. Der Name des **Pin** kann mit einem Klick editiert werden. Ein **Pin** kann mit dem *Delete Pin* Befehl entfert werden (Rechts-Klick auf die **Pin** Definition - aber nicht auf den Namen).

Der **Typ** eines **Subpipeline** **Pins** ist undefiniert, solange der **Pin** nicht verbunden ist. Sobald der **Pin** verbunden wird (entweder von innen oder von außen) wird sein **Typ** festgelegt (bestimmt durch den **Typ** des **Pins** am anderen Ende dieser Verbindung).
