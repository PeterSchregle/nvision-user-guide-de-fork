![](images/n-vision.png) 
========================

##Benutzerhanduch

**Machine Vision Entwicklungssystem**

![](images/monitor.png)

*Urheberrecht (c) 2015 Impuls Imaging GmbH*  
*Alle Rechte vorbehalten.* 

![](images/ImpulsLogo.png)

   
**Impuls Imaging GmbH**  
Schlingener Str. 4  
86842 Türkheim 

Deutschland/European Union

[http://www.impuls-imaging.com](http://www.impuls-imaging.com)

![](images/made-in-germany.png)
