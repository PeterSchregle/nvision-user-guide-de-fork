**nVision** kann Matrix-Codes dekodieren.

Verschiedene zwei-dimensionale Symbologien werden unterstützt. Hier ist
eine alphabetische Liste der unterstützten Symbologien:

<table>
<colgroup>
<col width="8%" />
<col width="10%" />
<col width="81%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Symbologie</th>
<th align="left">Dimensionalität</th>
<th align="left">link to description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Aztec</td>
<td align="left">2D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Aztec_Code"><a href="http://en.wikipedia.org/wiki/Aztec_Code" class="uri">http://en.wikipedia.org/wiki/Aztec_Code</a></a></td>
</tr>
<tr class="even">
<td align="left">Data Matrix</td>
<td align="left">2D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/Data_Matrix"><a href="http://en.wikipedia.org/wiki/Data_Matrix" class="uri">http://en.wikipedia.org/wiki/Data_Matrix</a></a></td>
</tr>
<tr class="odd">
<td align="left">PDF417</td>
<td align="left">1D stacked</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/PDF417"><a href="http://en.wikipedia.org/wiki/PDF417" class="uri">http://en.wikipedia.org/wiki/PDF417</a></a></td>
</tr>
<tr class="even">
<td align="left">QR Code</td>
<td align="left">2D</td>
<td align="left"><a href="http://en.wikipedia.org/wiki/QR_code"><a href="http://en.wikipedia.org/wiki/QR_code" class="uri">http://en.wikipedia.org/wiki/QR_code</a></a></td>
</tr>
</tbody>
</table>

Der Dekoder Knoten hat eine Selektionsliste, mit der die gewünschten
Symbologien für die Dekodierung ausgewählt werden können.

![Die Selektiionsliste der Symbologien für den Barcode
Dekoder.](images/2dsymbology_selection.png)

Der Dekoder erwartet einen Code in dem gegebenen Bild. Er erwartet den
Code in etwa in der Mitte.

Hier ist ein Beispiel für einen perfekt positionierten Code:

![Perfekt positionierter Code für den Dekoder.](images/data_matrix.png)

Der Matrix-Code Dekoder unternimmt keine weiteren Schritte, um einen
Code zu lokalisieren. Wenn Ihre Umstände anders sind, können Sie andere
Werkzeuge zur Vorverarbeitung verwenden, um den Matrix-Code in eine
günstige Position für den Dekoder zu bringen. Falls der Barcode
ungünstig positioniert ist, können Sie mit dem Crop Werkzeug den für den
Dekoder relevanten Bildteil ausschneiden. Falls Kontrast oder Helligkeit
nicht ausreichen, können Sie Bildvorverarbeitung vornehmen.

Der Dekoder Knoten kann in eine lineare Pipeline oder in eine
Sub-Pipeline eingefügt werden.

![In einer linearen Pipeline fließt das Bild von oben in den Dekoder und
die Symbologien können ausgewählt
werden.](images/marixcode_decoder_linear_pipeline.png)

In einer Sub-Pipeline gibt es zusätzliche Eingänge: Region für eine ROI,
bzw. TryHarder für aufwändigere Dekodierung, die aber auch länger dauern
kann.

![In einer Sub-Pipeline können mehrere Eingänge bessere Kontrolle
ausüben.](images/matrixcode_decoder_sub_pipeline.png)

Zusätzlich hat man auch am Ausgang des Dekoders mehrere Möglichkeiten.
Der Dekoder liefert eine Liste von Resultaten. Jedes Resultat enthält
die dekodierte Zeichenkeitte, den Namen und den Bezeichner der erkannten
Symbologie, sowie die Position, an der der Code gefunden wurde.
