**nVision** kann graphisch programmiert werden mittels des Konzepts
einer Bildverarbeitungs Pipeline.

Während ein Benutzer Bildverarbeitungsbefehle ausführt, baut **nVision**
eine Pipeline aus diesen Befehlen auf. Eine Pipeline ist ein Programm,
welches immer wieder ausgeführt werdne kann, möglicherweise für viele
Bilder. Die Bilddaten fließen durch die Pipeline, ähnlich wie Wasser
durch ein Rohrsystem fließt.

Hier ist ein Beispiel für eine Pipeline, in der die Daten von oben nach
unten fließen:

![Eine lineare Pipeline, in der die Daten von oben nach unten fließen,
in Richtung der Pfeile.](images/pipeline.png)

Es ist ausgesprochen einfach, diese Pipeline zu erzeugen, indem die
folgenden Befehle ausgeführt werden, einer nach dem anderen:

<table>
<colgroup>
<col width="20%" />
<col width="80%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="../images/file_open.png" alt="image0" /></td>
<td align="left">Öffnet eine Datei.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/thresholding.png" alt="image1" /></td>
<td align="left">Führt eine Segmentierung an einem Schwellwert durch.</td>
</tr>
<tr class="odd">
<td align="left"><img src="../images/connected_components.png" alt="image2" /></td>
<td align="left">Analysiert zusammenhängende Komponenten.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/blob_analysis.png" alt="image3" /></td>
<td align="left">Analysiert die Einzelobjekte.</td>
</tr>
</tbody>
</table>

Über einigen Pipeline Schritten gibt es einige Parameter, die geändert
werden können und die das Ergebnis der Pipeline beeinflussen. Sobald ein
Parameter durch den Benutzer geändert wird, läuft die Pipeline erneut
und die Ergebnisse werden neu berechnet.

Was Sie auf diese Weise gebaut haben ist eine lineare Sequenz von
Schritten - oder Pipeline Knoten - und dies ist im Prinzip ein sehr
einfaches Programm.

Pipeline
========

Eine lineare Pipeline ist die einfachste Art, ein Programm in
**nVision** zu erstellen.

Eine lineare Pipeline wird auf der linken Seite der Workbench angezeigt.
Jedes Bild kann seine eigene Pipeline haben, und sie ist eng mit dem
Bild verknüpft. Wenn ein Bild gespeichert wird, wird seine Pipeline mit
dem Bild mitgespeichert, unter demselben Namen mit angehängtem `.xpipe`
added. Wenn Sie ein Bild unter dem Namen `image.tif` speichern, wird
seine zugehörige Pipeline an derselben Stelle unter dem Namen
`image.tif.xpipe` gespeichert.

Eine Pipeline kann explizit in eine Datei exportiert werden, falls sie
für ein unterschiedliches Bild wiederverwendet werden soll. Die erfolgt
mit dem **Export Pipeline** Befehl aus dem **File** Menü. Um die
Pipeline für ein anderes Bild zu verwenden, benutzen Sie den Befehl
**Import Pipeline**, ebenfalls aus dem **File** Menü.

Eine Pipeline kann auch für einen Stapel an Bildern aus einem
Verzeichnis verwendet werden. In diesem Fall würden Sie zunächst mit dem
Befehl **Open Folder** aus dem **File** Menü den Bildstapel laden, und
anschließend mit **Import Pipeline** die Pipeline dazu. Auf dem
**Pipeline** Menü finden Sie die Befehle, mit denen Sie die Pipeline auf
den ganzen Stapel in einem Rutsch anwenden können, oder auch
Einzelschritte vorwärts oder rückwärts ausführen können.

Die Schritte in einer Pipeline nennen wir Pipeline Knoten oder einfach
Knoten.

![Ein Knoten in einer linearen Pipeline. Die Daten fließen oben in den
Knoten und unten heraus. Parameter können vom Benutzer mit den
Kontrollelementn oben am Knoten geändert werden. Die Ausgangsdaten
werden in einem kleinen Vorschaubild im Knoten
angezeigt.](images/node.png)

Hinter den Kulissen ist der Aufbau einer linearen Pipeline ein komplexer
Prozess. Wenn ein Befehl aus dem Menü ausgeführt wird, wird der
zugehörige Knoten an die Pipeline angehängt. Gleichzeitig wird er mit
der Pipeline verbunden, sodass die Daten bis ans neue Ende fließen
können. Außerdem wird die Visualisierung in der Workbench geändert,
sodass das Ergebnis des letzten Pipeline Knotens angezeigt wird.

Die Daten die durch die Pipeline fließen sind nicht alleine auf Bilder
beschränkt. Es gibt viele andere Datentypen, so wie Histogramme,
Profile, Regionen, Blob Analyse Eergebnisse oder sogar Zahlen. Befehle
sind jedoch oft nur für bestimmte Datentypen sinnvoll. Ein Filter
benötigt beispielsweise ein Bild an seinem Eingang. In der Konsequenz
werden manche Befehle auf dem Menü ausgegraut, wenn ihre Anwendung zu
einer inkonsistenten linearen Pipeline führen würde.

Üblicherweise ist der letzte Knoten in einer linearen Pipeline
ausgewählt, und neue Knoten werden am Ende angehängt. Sie können jedoch
auch einen Knoten in der Mitte der Pipeline selektieren. In diesem Fall
würde ein weiterer Knoten nach dem selektierten Knoten einfügt. In
diesem Fall sind oft sogar mehr Menübefehle ausgegraut, weil nicht nur
der Eingang des neuen Knotens vom Datentyp passen muss, sondern auch
sein Ausgang.

Eine weitere Eigenschaft der linearen Pipeline ist die Vorschau, die im
Knoten angezeigt wird. Wir haben versucht, diese Vorschau so hilfreich
wie möglich zu machen, damit es einfach für Sie wird, zu verstehen wie
eine spezielle Pipeline funktioniert. Die Vorschau ist life, was Sie
vermutlich schätzen werden, wenn sie eine Kamera angeschlossen haben.

Die Knoten in der linearen Pipeline haben ein Kontext-Menü, das
angezeigt wird, wenn Sie mit dem Mauszeiger über den Knoten fahren:

<table>
<colgroup>
<col width="17%" />
<col width="82%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="../images/visible.png" alt="image4" /></td>
<td align="left">Anzeigen des Ergebnisses des Knotens im Workbench Fenster.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/Video-Camera%2016x16.png" alt="image5" /></td>
<td align="left">Bildaufnahme von der Kamera starten und stoppen.</td>
</tr>
<tr class="odd">
<td align="left"><img src="../images/delete_small.png" alt="image6" /></td>
<td align="left">Den Knoten löschen.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/commit_document_small.png" alt="image7" /></td>
<td align="left">Die Ergebnisse des Knotens in den Workspace übertragen.</td>
</tr>
</tbody>
</table>

Wir möchten Sie ermuntern mit den Kommandos zu spielen und Pipelines
nach Lust und Laune zu bauen. **nVision** ist dafür gemacht, von Ihnen
entdeckt zu werden, und wir haben versucht, diese Entdeckungsphase so
einfach wie möglich zu machen. Sobald Sie ein paar Grundlagen wissen,
ist **nVision** sehr einfach erlernbar, und viele seiner Eigenschaften
können von Ihnen einfach erforscht werden, ohne dass Sie ein längliches
Handbuch lesen müssen.

Sub-Pipeline
============

Überraschend viele Anwendungen können mit einer linearen Pipeline gelöst
werden, aber früher oder später werden Sie auf eine Aufgabe stoßen, die
sie auf diese Arte nicht lösen können. Aus diesem Grund haben wir die
Möglichkeit geschaffen, mit verzweigten Pipelines zu arbeiten. Eine
verzweigte Pipeline kann mehrere Äste haben und sie ist viel
leistungsfähiger und flexibler als die lineare Variante. Hier ist ein
Beispiel für eine verzweigte Pipeline:

![Eine sehr einfache verzweigte Pipeline. Sie nimmt ein Bild auf ihrem
einzigen Eingang entgegen, transformiert es mittels einer Palette und
gibt das Ergebnis aus. Ein weiterer Knoten erzeugt die Palette aus einem
Satz an vordefinierten Einträgen. Unter der Pipeline sehen Sie das
Resultat: in diesem Fall ein
Falschfarben-Bild.](images/simple_branched_pipeline.png)

Die Zutaten für eine verzweigte Pipeline sind im wesentlichen gleich als
für die lineare Pipeline: Knoten und Verbindungen zwischen den Knoten.
Mit der höheren Flexibilität kommt auch ein wenig mehr Arbeit für den
Anwender: die Verbindungen werden nicht mehr automatisch gemacht, sonder
müssen manuell erfolgen.

![Ein Knoten in einer verzweigten Pipeline. Daten fließen von links in
den Knoten und rechts heraus. Parameter können mit den Kontrollelementen
links vom Knoten geändert werden. Die Ausgabedaten werden in einer
kleinen daumennagelgroßen Voransicht im Knoten
angezeigt.](images/branched_node.png)

Obwohl sie leicht unterschiedlich aussehen, sind die Knoten in der
linearen und der verzweigten Pipeline äquivalent. Sie können ebenfalls
sehen, dass die Unterscheidung in Eingang und Parameter etwas
willkürlich ist. Tatsächlich sind Parameter einfach zusätzliche
Eingänge.

Eingänge sind entweder obligatorisch oder optional. Obligatorische
Eingänge müssen mit anderen vorgeschalteten Knoten verbunden sein, sonst
kann der Knoten nicht ausgeführt werden. Wenn ein Knoten nicht
ausgeführt werden kann, zeigt er entweder eine Standard-Grafik, oder gar
nichts in seinem Vorschaubereich an. Optionale Eingänge müssen nicht
verbunden werden, und sie zeigen in dem Fall ein kleines
Kontrollelement, mit dem der Eingangswert gewählt werden kann. Wenn sie
jedoch mit einem vorgeschalteten Knoten verbunden sind, werden die Daten
von diesem Knoten genommen und das Kontrollelement wird nicht angezeigt.

Eine Sub-Pipeline wird mit dem **Subpipeline** Befehl vom **Pipeline**
Menü erzeugt. Dies fügt einen Sub-Pipeline Schritt zur linearen Pipeline
hinzu. Da die Sub-Pipeline zu diesem Zeitpunkt noch leer ist, zeigt die
Workbench ein leeres Fenster, aber oben sehen Sie einen Tab mit dem
Namen SubPipeline und einen orangen Splitter. Der Splitter kann mit der
Maus nach unten gezogen werden, und wenn Sie das mache, wird die obere
Fensterhälfte die Sub-Pipeline und die untere Fensterhälfte das\#
Ergebnis anzeigen. Da die Sub-Pipeline immer noch leer ist, ist in
beiden Fensterhälften nichts zu sehen. Klicken Sie auf den Namen der
Sub-Pipeline in der Vorschau, wenn Sie ihren Namen ändern wollen.

Innerhalb der Sub-Pipeline sind die Befehle über ein Kontext-Menü
wählbar, indem Sie mit der rechten Maustaste in das Editor Fenster
klicken. Es gibt wesentlich mehr Befehle als im linearen Fall.

![Das Kontext Menü der Sub-Pipeline.](images/context_menu.png)

Die Befehle im Kontext Menü sind in Gruppen eingeteilt. Die meisten
Befehle fügen einen Knoten zum Editorbereich hinzu, den Sie mit der Maus
im Fenster verschieben können, when sie in innerhalb seiner Box
anpacken. Sie können auch die Anschlüsse der Knoten mit denen anderer
Knoten verbinden, um den Datenfluss zu definieren. Pfeile können nur
zwischen kompatiblen Knoten gezogen werden, und kleien Icons führen Sie
und helfen Ihnen bei der Bestimmung kompatibler Anschlüsse.

Die Pipeline Gruppe enthält ein paar Befehle zum Umgang mit Pipelines.
Die Imaging Gruppe enthält Befehle zur Bildverarbeitung. Die Blob
Analysis Gruppe enthält Befehle zur Partikelanalyse. Die Gauging Gruppe
enthält Befehle zur Berechnung von Kantenpositionen mit
Subpixel-Genauigkeit. Die Template Matching Gruppe enthält Befehle zum
Mustervergleich und zur Mustersuche, mit oder ohne Rotation, mittels
normalisierter Korrelation oder geometrischem Matching. The Barcode
Gruppe enthält Befehler zur Dekodierung von Barcodes und Matrix Codes.
Die Graphik Gruppe enthält Befehler für graphische Overlays und
Editoren, und die Support Gruppe enthält viele andere Befehle zur
Erstellung von effizienten Pipelines.

Pipeline
--------

This group contains commands related to pipeline editing.

<table>
<colgroup>
<col width="11%" />
<col width="17%" />
<col width="71%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="../images/input-port.png" alt="image8" /></td>
<td align="left">Add Input Port</td>
<td align="left">Fügt einen weiteren Eingang hinzu.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/output-port.png" alt="image9" /></td>
<td align="left">Add Output Port</td>
<td align="left">Fügt einen weiteren Ausgang hinzu.</td>
</tr>
<tr class="odd">
<td align="left"><img src="../images/sub_pipeline.png" alt="image10" /></td>
<td align="left">Sub-Pipeline</td>
<td align="left">Fügt einen Sub-Pipeline Knoten ein.</td>
</tr>
</tbody>
</table>

Eingänge und Ausgänge können durch einen Klick mit der rechten Maustaste
editiert und geändert werden. Hinzugefügte Ein- und Ausgänge können
gelöscht werden, indem mit der rechten Maustaste auf ihren Verbinder
geklickt wird und Delete aus dem Menü ausgewählt wird.

Mit dem Sub-Pipeline Befehl wird ein Sub-Pipeline Knoten eingefügt. Ein
Doppelklick auf den Knoten öffnet den Editor eine Ebene tiefer in der
Hierarchie. Eine Brotkrümel-Navigation an der Oberseite des Editors
zeigt Namen und Verschachtelungstiefe. Mit der Brotkrümel-Navigation
können Sie auf höhere Ebenen wechseln. Die Namen der Sup-Pipelines
können durch Klicken auf ihren Titel im Pipeline Editor geändert werden.

Imaging
-------

Die Befehle in dieser Gruppe sind in Untermenüs eingeteilt, entsprechend
dem Typ, der als Eingang verwendet wird. Befehle, die ein Bild verwendet
sind im Image Untermenü, usw. Der Befehl zur Berechnung eines
Histogramms findet sich deshalb in Image -\> Statistik, wohingegen
Befehle, die ein Histogramm verwenden im Histogram Menü auftauchen (z.B.
der Befehl Osu Threshold zur Berechnung einer optimalen Schwelle aus den
Histogrammdaten).

<table>
<colgroup>
<col width="11%" />
<col width="19%" />
<col width="69%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">Buffer</td>
<td align="left">Gruppiert Buffer Befehle.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Histogram</td>
<td align="left">Gruppiert Histogramm Befehle.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Image</td>
<td align="left">Gruppiert Bild Befehle.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Palette</td>
<td align="left">Gruppiert Paletten Befehle.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Regions</td>
<td align="left">Gruppiert Regionen Befehle.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Structuring Element</td>
<td align="left">Gruppiert Structurelemente Befehle.</td>
</tr>
</tbody>
</table>

Blob Analysis
-------------

Die Befehle in dieser Gruppe beziehen sich auf Blob Analyse, d.h. die
Berechnung von Werten aus segmentierten Objekten.

<table>
<colgroup>
<col width="11%" />
<col width="20%" />
<col width="68%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="../images/blob_analysis.png" alt="image11" /></td>
<td align="left">Blob Analysis</td>
<td align="left">Berechnet einen Satz an Features für eine Liste von Regionen.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Blob Feature</td>
<td align="left">Berechnet ein einzelnes Feature für eine einzelne Region.</td>
</tr>
<tr class="odd">
<td align="left"><img src="../images/save.png" alt="image12" /></td>
<td align="left">Export Blob Analysis</td>
<td align="left">Speichter einen Satz an Features in eine CSV Datei.</td>
</tr>
</tbody>
</table>

Gauging
-------

Die Befehle in dieser Gruppe beziehen sich auf Vermessung, d.h. die
Berechnung von Sub-Pixel genauen Messungen aus Bildkanten.

<table>
<colgroup>
<col width="11%" />
<col width="20%" />
<col width="68%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="../images/gauge_hpos.png" alt="image13" /></td>
<td align="left">Edge Position</td>
<td align="left">Berechnet die horizontale oder vertikale Position einer Kante.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/gauge_vpos.png" alt="image14" /></td>
<td align="left">Edge Distance</td>
<td align="left">Berechnet die horizontale oder vertikale Distance zweier Kanten.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Measure Line</td>
<td align="left">Berechnet eine schiefe Linie entlang einer Kante.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Measure Points</td>
<td align="left">Berechnet eine Liste von Punkten entlang von Bildkanten.</td>
</tr>
</tbody>
</table>

Template Matching
-----------------

Die Befehle in dieser Gruppe beziehen sich auf Mustervergleich, d.h. die
Berechnung von Sub-Pixel genauen Positionen aus Mustern in Bildern.

<table>
<colgroup>
<col width="10%" />
<col width="26%" />
<col width="62%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">Template Matching</td>
<td align="left">Vergleicht zwei Muster.</td>
</tr>
<tr class="even">
<td align="left"><img src="../images/search.png" alt="image15" /></td>
<td align="left">Template Search</td>
<td align="left">Sucht ein Muster in einem Bild.</td>
</tr>
<tr class="odd">
<td align="left"><img src="../images/search_with_rotation.png" alt="image16" /></td>
<td align="left">Template Search (With Rotation)</td>
<td align="left">Sucht ein Muster mit möglicher Rotation in einem Bild.</td>
</tr>
</tbody>
</table>

Barcode
-------

Die Befehle in dieser Gruppe beziehen sich auf Barcode Dekodierung.

<table>
<colgroup>
<col width="11%" />
<col width="20%" />
<col width="68%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="../images/barcode.png" alt="image17" /></td>
<td align="left">Decode Barcode</td>
<td align="left">Decodiert einen Barcode in einem Bild.</td>
</tr>
</tbody>
</table>

Graphics
--------

Die Befehle in dieser Gruppe beziehen sich auf Grafik.

<table>
<colgroup>
<col width="11%" />
<col width="19%" />
<col width="69%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">CAD Drawing</td>
<td align="left">Befehle zum Import von CAD Zeichnungen und deren Umwandlung in Bilder.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Geometry</td>
<td align="left">Befehler für geometrische Objekte.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Widgets</td>
<td align="left">Erzeugt interaktive grafische Elemente zur Darstellung und Eingabe.</td>
</tr>
</tbody>
</table>

Support
-------

Die Kommandos in dieser Gruppe unterstützen die Erstellung von
Pipelines.

<table>
<colgroup>
<col width="11%" />
<col width="19%" />
<col width="69%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">Core</td>
<td align="left">Kern-Befehle für Mathematic, Logik, Listenverarbeitung, usw.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Digital IO</td>
<td align="left">Kommandos für digitale Ein- und Ausgabe.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Tools</td>
<td align="left">Enthält Werkzeuge zur interaktiven Nutzung.</td>
</tr>
</tbody>
</table>

Lists
=====

Pipelines nach dem Datenfluss Prinzip haben keine Schleifen. So wie
Wasser in einem Rohrsystem auch nicht in Schleifen fließt, gibt es auch
in unseren Pipelines kein vor und zurück für die Daten. Wasser fließt
nur in einer Richtung, von der Quelle zur Mündung. Was sich wie eine
ernsthafte Einschränkung anhören könnte, ist in Wahrheit ein großer
Vorteil: das Fehler von Schleifen führt zu weniger Fehlern. Fehlerhafte
Schleifen sind einer der populärsten Programmierfehler und verursachen
viel unnötige Arbeit, um sie auszumerzen.

Listen-Verarbeitung ist das Konzept das an ihre Stelle tritt. Schleifen
in traditionellen Programmen werden in nVision durch Verarbeitung von
Listen ersetzt. Listen können auf verschiedene Weise manipuliert werden,
und die genaue Art dieser Manipulation wird durch graphische Pipelines
definiert, wie alles andere in nVision.

<table>
<colgroup>
<col width="11%" />
<col width="19%" />
<col width="69%" />
</colgroup>
<thead>
<tr class="header">
<th align="left"></th>
<th align="left">Kommando</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"></td>
<td align="left">Get Item</td>
<td align="left">Einzelnes Objekt aus einer Liste fischen.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Filter (Where)</td>
<td align="left">Eine Liste mit einer Bedingung filtern.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Take While</td>
<td align="left">Objekte aus einer Liste übernehmen, solange eine Bedingung wahr ist.</td>
</tr>
<tr class="even">
<td align="left"></td>
<td align="left">Skip While</td>
<td align="left">Objekte aus einer Liste überspringen, solange eine Bedingung wahr ist.</td>
</tr>
<tr class="odd">
<td align="left"></td>
<td align="left">Transform (Select)</td>
<td align="left">Eine Liste von Objekten eines Typs in eine Liste anderen Typs umwandeln.</td>
</tr>
</tbody>
</table>

Die Listen-Knoten schauen oft wie spezialisierte Sub-Pipelines aus.
