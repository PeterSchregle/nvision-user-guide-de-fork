**nVision** kann CAD Zeichnungen importieren und sie in Bilder
konvertieren. Sobald eine Zeichnung in ein Bild konvertiert wurde,
können Bildverarbeitungsfunktionen angewandt werden. Anwendungen von
besonderem Interesse sind:

-   CAD Zeichnungen mit aufgenommenen Bildern vergleichen, und
    Unterschiede inspizieren.
-   CAD Zeichnungen verwenden, um ROI (regions of interest) für
    Teileinspektion zu setzen.

**nVision** unterstützt den Zeichnungsimport von Autocad DXF Dateien und
Trumpf GEO Dateien.

DXF Dateiimport
===============

Das DXF format wurde von Autodesk
([<http:www.autodesk.com>](http://www.autodesk.com)) definiert und
weiterentwickelt. Durch seine Verwendung in AutoCad ist es def de-facto
Standard zum Zeichnungsaustausch geworden, sogar zwischen verschiedenen
Herstellern. Das DXF Format ist zwar syntaktisch einfach und leicht zu
dekodieren, seine Semantik ist jedoch kompliziert, weil das Format
diesbezüglich nicht besonders gut dokumentiert ist. Außerdem wurde es
über viele Jahre geändert und seit den ersten AutoCAD Versionen auf eine
ad-hoc Art und Weise erweitert. Dieses Erbe macht die Unterstützung des
Formats schwierig.

Wir können deshalb nicht einfach konstatieren, dass der Zeichnungsimport
immer und in jedem Fall funktionieren wird. **nVision** unterstützt nur
eine partielle 2D Untermenge. Folgende Primitive sind unterstützt:

<table>
<colgroup>
<col width="16%" />
<col width="26%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Primite</th>
<th align="left">Eigenschaft</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">line</td>
<td align="left">Liniensegment</td>
</tr>
<tr class="even">
<td align="left">arc</td>
<td align="left">Kreisbogen</td>
</tr>
<tr class="odd">
<td align="left">circle</td>
<td align="left">Kreis</td>
</tr>
<tr class="even">
<td align="left">polygon</td>
<td align="left">Polygon</td>
</tr>
</tbody>
</table>

Dateien mit anderen Primitiven können üblicherweise gelesen werden, aber
die unbekannten Primitiven werden ignoriert.

Zum Import von DXF Dateien muss der Dateiname angegeben werden und auch
der Layername, von dem importiert werden soll. Falls mehrere Layer
importiert werden sollen, können mehrere Import Knoten parallel
verwendet werden.

![](images/import_dxf.png)

GEO Dateiimport
===============

Das GEO Format wird von Laserschneidmaschinen von TRUMPF
([<http:www.trumpf.com>](http://www.trumpf.com)) verwendet.

**nVision** unterstützt eine 2D Untermenge.

<table>
<colgroup>
<col width="16%" />
<col width="26%" />
</colgroup>
<thead>
<tr class="header">
<th align="left">Primite</th>
<th align="left">Eigenschaft</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">line</td>
<td align="left">Liniensegment</td>
</tr>
<tr class="even">
<td align="left">arc</td>
<td align="left">Kreisbogen</td>
</tr>
<tr class="odd">
<td align="left">circle</td>
<td align="left">Kreis</td>
</tr>
</tbody>
</table>

Dateien mit anderen Primitiven können üblicherweise gelesen werden, aber
die unbekannten Primitiven werden ignoriert.

Zum Import von GEO Dateien muss der Dateiname angegeben werden.

![](images/import_geo.png)

Rendern von CAD Zeichnungen
===========================

Beide Import Knoten erzeugen eine CAD Zeichnung, wenn sie erfolgreich
laufen. Die Zeichnung hat Eigenschaften, mit denen weitere Informationen
herausgefunden werden können, wie z.B. Höhe und Breite (in mm) sowie die
enthaltenen Konturen.

Außerdem kann die Zeichnung in ein Bild gerendert werden.

![](images/render_cad_drawing.png)

Wenn eine CAD Zeichnung gerendert wird, kann mit dem Parameter
Resolution angegeben werden, wie fein das Rendering sein soll. Ein Wert
von 1 bedeutet, dass ein Pixel eine Größe (Fläche) von 1 mm (1mm²) hat.
Ein Wert von 0.1 bedeutet, dass ein Pixel eine Größe (Fläche) von 0.1 mm
(0.01 mm²) hat. Die Größe des gerenderten Bilds wird durch Höhe und
Breite der importierten CAD Zeichnung (in mm) bestimmt, geteilt durch
den Parameter Resolution. Je kleiner (feiner) der Parameter Resolution,
umso größer wird das gerenderte Bild.

Der Parameter Fill bestimmt, ob die Zeichnungskonturen gefüllt oder
umrandet gerendert werden.
