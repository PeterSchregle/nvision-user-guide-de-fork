Der **Transform** **Node** nimmt eine Liste von Dingen und transformiert diese in eine Liste von anderen Dingen.

![](./images/transform.png)

**Transform** geht einzeln durch die List, und transformiert jedes Ding einzeln in ein neues Ding. Die neuen Dinge werden anschließend zur Ausgabeliste zusammengefügt.

Von außen sieht ein **Transform** **Node** ähnlich wie eine **Subpipeline** aus. Wie jeder andere **Node** hat ein **Transform** **Node** optionale **Input Pins** und/oder **Output Pins**. 

Standardmäßig hat der **Transform** **Node** einen **Input Pin** und einen **Output Pin**.

Der Name des **Transform** **Node** editiert werden, wenn er angeklickt wird:

![](./images/subpipeline_title_edit.png)

Im **Transform** **Node** (Doppel-Klick auf den **Node** um hineinzugehen) sieht es so aus:

![](./images/subpipeline_inside.png)

Innen sieht der **Transform** **Node** ählich wie eine **Subpipeline** aus, jedoch hat er vordefinierte **Input Pins** *Item* und *ItemIndex*. An diesen vordefinierten **Pins** liegen jeweils ein Ding aus der Eingangsliste, sowie sein Index in der Eingangsliste.

Zusätzliche **Pins** eines **Transform** **Node** können mit den *Add Input Pin* und *Add Output Pin* Befehlen im Kontextmenü definiert werden. Der Name des **Pin** kann mit einem Klick editiert werden. Ein **Pin** kann mit dem *Delete Pin* Befehl entfert werden (Rechts-Klick auf die **Pin** Definition - aber nicht auf den Namen).

Der **Typ** eines **Transform** **Pins** ist undefiniert, solange der **Pin** nicht verbunden ist. Sobald der **Pin** verbunden wird (entweder von innen oder von außen) wird sein **Typ** festgelegt (bestimmt durch den **Typ** des **Pins** am anderen Ende dieser Verbindung).