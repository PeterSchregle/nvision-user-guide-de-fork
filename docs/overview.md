Die Abbildung zeigt die verschiedenen Bereiche der **nVision**
Anwendung.

![Die verschiedenen Bereiche der **nVision**
Anwendung.](images/nVisionGui.png)

Oben befindet sich ein Befehlsband mit Kommandos zur Steuerung der
Anwendung.

Unten ist der Arbeitsbereich, der aus dem Browser und der Workbench
besteht, und in dem die hauptsächlichen Interaktionen stattfinden.

Legen wir los
=============

Bevor wir **nVision** im Detail erklären, wollen wir mit ein paar
einfachen Beispielen beginnen, die zeigen, wie man mit **nVision**
arbeiten kann.

Partikelanalyse
---------------

Nehmen wir an, dass unsere Aufgabe das Zählen und Vermessen von
Partikeln wäre.

**1. Das Bild öffnen**

Der erste Schritt ist ein Bild zu laden. **nVision** kann Bilder von
digitalen Kameras aufnehmen, aber zu Beginn wollen wir ein Bild von der
Platte laden, mit dem **File - Open** Befehl:

![Ein Bild von der Platte laden.](images/file-open.png)

Im Dialog wählen wir die Datei `cells.tif` aus. The Datei wird geladen
und sowohl im Browser als auch im Arbeitsbereich angezeigt.

![**nVision** mit einem geladenen Bild.](images/cells-loaded.png)

**2. Das Bild segmentieren**

Der nächste Schritt ist die Segmentierung des Bildes. Die Partikel - in
diesem Fall die Zellkerne - sollen vom Hintergrund separiert werden.
Dies kann mit dem **Threshold** Befehl von der Seite **Segmentation**
erfolgen.

![Ein Bild mit dem Threshold Befehl segmentieren.](images/threshold.png)

Eine Schwelle von `100` funktioniert prima mit diesem Bild.

![Das Ergebnis der Segmentierung ist dem Bild rot
überlagert.](images/cells-thresholded.png)

Natürlich können Sie den Modus als auch die Schwelle mit den
Eingabefeldern über dem Threshold Knoten ändern.

Das Aussehen der Bilddarstellung hat sich verändert, weil die
segmentierten Zellkerne in roter Farbe (teilweise transparent) dem
Originalbild überlagert werden. Außerdem wurde ein Threshold Knoten der
linearen Verarbeitungs Pipeline hinzugefügt, die jetzt aus zwei
Schritten besteht.

**3. Analyse des Zusammenhangs**

Wählen Sie den Befehl **Connected Components** von der Seite
**Segmentation**.

![Eine Region in zusammenhängende Komponenten
trennen.](images/connected-components.png)

Der Befehl führt eine Zusammenhangsanalyse aus und teilt das
Segmentierungs-Ergebnis des vorigen Schrittes in verschiedenen
zusammenhängende Objekte.

![Das Ergebnis der Komponenten-Trennung ist dem Bild farbig
überlagert.](images/cells-connected-components.png)

Das Aussehen der Bilddarstellung hat sich wieder verändert: die
verschiedenen Zellkerne sind jetzt in verschiedenen Farben dargestellt.
Außerdem wurde ein weiterer Knoten zur linearen Pipeline hinzugefügt.

**4. Blob-Analyse**

Der letzte Schritt in unserem einführenden Beispiel ist die Analyse der
Zellkerne.

Wählen Sie dazu den Befehl **Blob Analysis** aus der Seite
**Segmentation**.

![Die Objekte analysieren.](images/blob-analysis.png)

Ein weiterer Schritt wurde zur Pipeline hinzugefügt, der die Anzahl
aller Blobs anzeigt und eine mit Zahlen gefüllte Tabelle wurde unter dem
Bild angezeigt.

![Die Ergebnisse der Blob Analyse.](images/cells-blob-analysis.png)

Für jedes Objekt im Bild wird eine Zeile in der Tabelle angezeigt mit
den Messwerten (auch Features genannt) für das jeweilige Partikel.
Zwischen dem Bild und der Tabelle ist eine Zeile mit ein paar
Kontrollkästchen: **Bounds** und **EquivalentEllipse**. Diese Features
können graphisch visualisiert werden.

Stellen Sie sicher, dass Sie entweder **Bounds** oder
**EquivalentEllipse** ausgewählt haben und klicken Sie das **Show**
Kontrollkästchen in der ersten Spalte einer beliebigen Tabellenzeile.
Dadurch wird entweder das umgebende Rechteck oder die äquivalente
Ellipse des ausgewählten Objekts graphisch angezeigt.

Sie können auch die graphischen Objekte im Bild anklicken und die
Tabelle blättert zur Zeile, die zu dem angeklickten Objekt gehört.

Außerdem können Sie den Befehl **Commit** aus dem Befehlsband auswählen,
um einen Schnappschuss der Messwerte in das Browser-Fenster zu
übertragen. Von dort können die Daten in eine CSV Datei (comma separated
values, Komma separierte Werte) exportiert werden, die in Excel oder
andere Programme zur Weiterverarbeitung geladen werden kann.

nVision hat eine Pipeline von Kommandos aufgebaut, als Sie die Befehle
ausgeführt haben. Diese Pipeline kann wiederverwendet werden und auf
andere Bilder angewandt werden. In Wirklichkeit haben Sie ein kleines
Programm erstellt, aber ohne dass Sie programmieren mussten. Wenn Sie
**nVision** beenden, wird diese Pipeline zusammen mit dem Bild
gespeichert (wenn das Bild cells.tif heißt, dann bekommt die zugeordnete
Pipeline den Namen cells.tif.xpipe).

Wir hoffen, dass diese kleine Anleitung Ihren Appetit auf mehr angeregt
hat und dass Sie weiterlesen und mit **nVision** arbeiten werden. Viel
Vergnügen!
