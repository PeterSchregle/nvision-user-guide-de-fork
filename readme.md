Building the nVision Benutzerhandbuch
-------------------------------------

Link to the nVision User Guide: [http://nvision-user-guide-de.readthedocs.org/](http://nvision-user-guide-de.readthedocs.org/ "nvision-user-guide-de.readthedocs.org")

## Tools

[Markdown](http://daringfireball.net/projects/markdown/ "Markdown") is used to write the documentation.

[Mkdocs](http://www.mkdocs.org) is used to build the documntation.

[Bitbucket](http://bitbucket.org "Bitbucket") is used to store the documentation sources in a public repository.

[ReadTheDocs](http://readthedocs.org "ReadTheDocs") is used to build and serve the built documenation to the world.

## Process 

1. Documentation is changed in the private [nvision-user-guide-de](https://bitbucket.org/impuls-imaging/nvision-user-guide-de.git "nvision-user-guide") repository and committed and pushed.
2. The public fork [nvision-user-guide-de-fork](http://https://PeterSchregle@bitbucket.org/PeterSchregle/nvision-user-guide-de-fork.git "nvision-user-guide-de-fork") is synced manually.
3. Bitbucket notifies ReadTheDocs because the ngi-user-guide-fork directory has set a ReadTheDocs hook.
4. ReadTheDocs builds the documentation and serves it at [http://nvision-user-guide-de.readthedocs.org/](http://nvision-user-guide-de.readthedocs.org/ "nvision-user-guide-de.readthedocs.org").

## Setup

### Setup Mkdocs

Follow the instructions at [http://www.mkdocs.org](http://www.mkdocs.org) if you want to build documentation locally.

### Repositories

The private repository for the documentation is 

	https://PeterSchregle@bitbucket.org/PeterSchregle/nvision-user-guide-de.git

A public fork of it is

	http://bitbucket.org/PeterSchregle/nvision-user-guide-de-fork.git

ReadTheDocs needs public access so that it can grab the files. The esiest way I found was to create a fork with public access.

### Setup ReadTheDocs

The important setting is the repository:

	http://bitbucket.org/PeterSchregle/nvision-user-guide-de-fork.git
